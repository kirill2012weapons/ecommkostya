<?php
/**
 * Template Name: Find Order Custom
 */
?>

<?php

use KoshTheme\Forms\User\Checkout\CheckoutFindOrderModel;
use KoshTheme\Forms\User\Checkout\CheckoutFindOrderType;


$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$checkoutFindOrderModel = new CheckoutFindOrderModel();

$checkoutFindOrderForm = $formFactory
  ->createBuilder(CheckoutFindOrderType::class, $checkoutFindOrderModel)
  ->getForm();

$checkoutFindOrderForm->handleRequest($request);

if ($checkoutFindOrderForm->isSubmitted() && $checkoutFindOrderForm->isValid()) {
  $order = wc_get_order($checkoutFindOrderModel->getOrderId());
  $orderHash = $checkoutFindOrderModel->getOrderHash();
}

$checkoutFindOrderFormView = $checkoutFindOrderForm->createView();

?>

<?php get_header(); ?>

<div class="ps-checkout pt-80 pb-80">
  <div class="ps-container">

    <?php
    echo render('checkout/thankyou_form.php', [
      'checkoutFindOrderFormView' => $checkoutFindOrderFormView
    ]);
    ?>

    <?php if (isset($order) && isset($orderHash)) : ?>
      <?php
      echo render('checkout/thankyou.php', [
        'order' => $order,
        'orderHash' => $orderHash
      ]);
      ?>
    <?php endif; ?>

  </div>
</div>

<?php get_footer(); ?>
