<?php
/**
 * Template Name: TELEGRAM BOT PAGE
 */
?>

<?php

try {

  $telegram = container()->get('telega.bot');

  $telegram->handle();

//  container()->get('log.debug')->info('Incoming...');

} catch (Longman\TelegramBot\Exception\TelegramException $e) {
  container()->get('log.debug')->info($e->getMessage() . ' - ' . $e->getFile());
} catch (Exception $e) {
  container()->get('log.debug')->info($e->getMessage() . ' - ' . $e->getFile());
}