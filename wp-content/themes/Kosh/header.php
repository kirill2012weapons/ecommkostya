<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie ie8"><![endif]-->
<!--[if IE 9]>
<html class="ie ie9"><![endif]-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="<?= get_theme_root_uri(); ?>/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="<?= get_theme_root_uri() . '/Kosh_html'; ?>/favicon.png" rel="icon">

  <meta name="author" content="Nghia Minh Luong">
  <meta name="keywords" content="Default Description">
  <meta name="description" content="Default keyword">

  <title>Sky - Homepage</title>

  <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow:300,400,700%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">

  <?php wp_head(); ?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<!--[if IE 7]>
<body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 8]>
<body class="ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 9]>
<body class="ie9 lt-ie10"><![endif]-->

<body class="ps-loading">

<div class="header--sidebar"></div>

<header class="header">
  <div class="header__top">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-6 col-xs-12 ">

          <?php if (get_field('mpl_g_title_header', 'opt_st_id')) : ?>
            <p><?php echo get_field('mpl_g_title_header', 'opt_st_id') ?></p>
          <?php endif; ?>

        </div>
        <?php if (is_user_logged_in()) : ?>
          <?php
          /**
           * @var $currentUser WP_User
           */
          $currentUser = wp_get_current_user();
          ?>
          <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 ">
            <div class="header__actions">
              <a href="#cart-popup" class="open-popup-link">КОРЗИНА</a>
              <a href="<?php echo home_url('/account/'); ?>">
                <?= $currentUser->display_name; ?>
              </a>
              <a href="<?= wp_logout_url(home_url()); ?>">
                Выйти
              </a>
            </div>
          </div>
        <?php else: ?>
          <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 ">
            <div class="header__actions">
              <a href="#cart-popup" class="open-popup-link">КОРЗИНА</a>
              <a href="<?= home_url('/register/') ?>">Регистрация / Войти</a>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <nav class="navigation">
    <div class="container-fluid">
      <div class="navigation__column left">
        <div class="header__logo"><a class="ps-logo" href="<?= home_url(); ?>"><img src="<?= HTML_IMG; ?>/logo.png" alt=""></a></div>
      </div>
      <div class="navigation__column center">
        <ul class="main-menu menu">
          <li class="menu-item">
            <a href="<?= home_url(); ?>">Главная</a>
          </li>
          <li class="menu-item">
            <a href="<?php echo get_post_type_archive_link('blog_pt') ?>">Новости</a>
          </li>
          <li class="menu-item">
            <a href="<?php echo home_url('/contact/') ?>">Контакты</a>
          </li>
          <li class="menu-item">
            <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">Магазин</a>
            <?php if (false) : ?>
              <div class="mega-menu">
              <div class="mega-wrap">
                <div class="mega-column">
                  <ul class="mega-item mega-features">
                    <li><a href="product-listing.html">NEW RELEASES</a></li>
                    <li><a href="product-listing.html">FEATURES SHOES</a></li>
                    <li><a href="product-listing.html">BEST SELLERS</a></li>
                    <li><a href="product-listing.html">NOW TRENDING</a></li>
                    <li><a href="product-listing.html">SUMMER ESSENTIALS</a></li>
                    <li><a href="product-listing.html">MOTHER'S DAY COLLECTION</a></li>
                    <li><a href="product-listing.html">FAN GEAR</a></li>
                  </ul>
                </div>
                <div class="mega-column">
                  <h4 class="mega-heading">Shoes</h4>
                  <ul class="mega-item">
                    <li><a href="product-listing.html">All Shoes</a></li>
                    <li><a href="product-listing.html">Running</a></li>
                    <li><a href="product-listing.html">Training & Gym</a></li>
                    <li><a href="product-listing.html">Basketball</a></li>
                    <li><a href="product-listing.html">Football</a></li>
                    <li><a href="product-listing.html">Soccer</a></li>
                    <li><a href="product-listing.html">Baseball</a></li>
                  </ul>
                </div>
                <div class="mega-column">
                  <h4 class="mega-heading">CLOTHING</h4>
                  <ul class="mega-item">
                    <li><a href="product-listing.html">Compression & Nike Pro</a></li>
                    <li><a href="product-listing.html">Tops & T-Shirts</a></li>
                    <li><a href="product-listing.html">Polos</a></li>
                    <li><a href="product-listing.html">Hoodies & Sweatshirts</a></li>
                    <li><a href="product-listing.html">Jackets & Vests</a></li>
                    <li><a href="product-listing.html">Pants & Tights</a></li>
                    <li><a href="product-listing.html">Shorts</a></li>
                  </ul>
                </div>
                <div class="mega-column">
                  <h4 class="mega-heading">Accessories</h4>
                  <ul class="mega-item">
                    <li><a href="product-listing.html">Compression & Nike Pro</a></li>
                    <li><a href="product-listing.html">Tops & T-Shirts</a></li>
                    <li><a href="product-listing.html">Polos</a></li>
                    <li><a href="product-listing.html">Hoodies & Sweatshirts</a></li>
                    <li><a href="product-listing.html">Jackets & Vests</a></li>
                    <li><a href="product-listing.html">Pants & Tights</a></li>
                    <li><a href="product-listing.html">Shorts</a></li>
                  </ul>
                </div>
                <div class="mega-column">
                  <h4 class="mega-heading">BRAND</h4>
                  <ul class="mega-item">
                    <li><a href="product-listing.html">NIKE</a></li>
                    <li><a href="product-listing.html">Adidas</a></li>
                    <li><a href="product-listing.html">Dior</a></li>
                    <li><a href="product-listing.html">B&G</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <?php endif; ?>
          </li>
          <?php if (false) : ?>
          <li class="menu-item menu-item-has-children dropdown"><a href="#">News</a>
            <ul class="sub-menu">
              <li class="menu-item menu-item-has-children dropdown"><a href="blog-grid.html">Blog-grid</a>
                <ul class="sub-menu">
                  <li class="menu-item"><a href="blog-grid.html">Blog Grid 1</a></li>
                  <li class="menu-item"><a href="blog-grid-2.html">Blog Grid 2</a></li>
                </ul>
              </li>
              <li class="menu-item"><a href="blog-list.html">Blog List</a></li>
            </ul>
          </li>
          <?php endif; ?>
          <li class="menu-item"><a href="<?php echo home_url('/order-finder/') ?>">Статус Заказа</a></li>
        </ul>
      </div>
      <div class="navigation__column right">

        <?php if (false) : ?>

        <form class="ps-search--header" action="do_action" method="post">
          <input class="form-control" type="text" placeholder="Search Product…">
          <button><i class="ps-icon-search"></i></button>
        </form>

        <?php endif; ?>

        <?php echo render('mini-card/card-base.php'); ?>

        <div class="menu-toggle"><span></span></div>
      </div>
    </div>
  </nav>
</header>

<?php echo render('parts/header_services.php'); ?>

<main class="ps-main">