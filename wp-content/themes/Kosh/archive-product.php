<?php get_header(); ?>

<?php

use KoshTheme\Forms\ShopFilter\FilterModel;
use KoshTheme\Forms\ShopFilter\FilterType;


$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$filterModel = new FilterModel();
$filterModel->setPriceMin(0);
$filterModel->setPriceMax(2000);

$filterForm = $formFactory
  ->createBuilder(FilterType::class, $filterModel)
  ->getForm();

$filterForm->handleRequest($request);

$filterFormView = $filterForm->createView();

$taxArray = [
  'tax_query' => [
    'relation' => 'AND'
  ]
];

$metaArray = [
  'meta_query' => [
    'relation' => 'AND'
  ]
];

$argsSearch = array(
  'post_type' => 'product',
  'post_status' => 'publish',
  'ignore_sticky_posts' => true,
  'posts_per_page' => 12,
  'paged' => $paged,
);

if (!empty($filterModel->getSortedBy())) {
  switch ($filterModel->getSortedBy()) {
    case 'date_desc' :
      $argsSearch['orderby'] = 'date';
      $argsSearch['order'] = 'DESC';
      break;
    case 'date_asc' :
      $argsSearch['orderby'] = 'date';
      $argsSearch['order'] = 'ASC';
      break;
    case 'price_desc' :
      $argsSearch['orderby'] = 'meta_value_num';
      $argsSearch['meta_key'] = '_price';
      $argsSearch['order'] = 'DESC';
      break;
    case 'price_asc' :
      $argsSearch['orderby'] = 'meta_value_num';
      $argsSearch['meta_key'] = '_price';
      $argsSearch['order'] = 'ASC';
      break;
    default :
      $argsSearch['orderby'] = 'date';
      $argsSearch['order'] = 'DESC';
      break;
  }
}

if (!empty($filterModel->getCategories())) {
  $taxArray['tax_query'][] = [
    'taxonomy' => 'product_cat',
    'field' => 'slug',
    'terms' => $filterModel->getCategories(),
  ];
}

if (!empty($filterModel->getTint())) {
  $taxArray['tax_query'][] = [
    'taxonomy' => 'tint_tax',
    'field' => 'slug',
    'terms' => $filterModel->getTint(),
  ];
}

if (!empty($filterModel->getColor())) {
  $taxArray['tax_query'][] = [
    'taxonomy' => 'color_tax',
    'field' => 'slug',
    'terms' => $filterModel->getColor(),
  ];
}

$metaArray['meta_query'][] = [
  'key' => '_price',
  'value' => [ $filterModel->getPriceMin(), $filterModel->getPriceMax() ],
  'compare' => 'BETWEEN',
  'type' => 'NUMERIC'
];
$metaArray['meta_query'][] = [
  'key' => '_stock_status',
  'value' => 'instock',
  'compare' => '='
];

$argsSearch = array_merge($argsSearch, $taxArray);
$argsSearch = array_merge($argsSearch, $metaArray);

try {
  $catFilter = container()->get('shop.filter.category');
} catch (Exception $e) {
  dump($e->getMessage());
}

$products = new WP_Query($argsSearch);

?>

<form action="?" method="get" data-form-sbm class="ps-products-wrap pt-80 pb-80">
  <div class="ps-products" data-mh="product-listing">
    <div data-form-sbm-load-triger style="display: none; text-align: center;">
      <img src="<?php echo PHP_IMG ?>/loadearth.svg" alt="">
    </div>
    <div data-form-sbm-load-triger class="ps-product-action">

      <?php echo render('service/shop/components/sotred-filter.php', [
        'sortedBy' => $filterFormView->children['sortedBy'],
      ]) ?>

      <?php
      $big = 999999999;
      echo paginate_links_custom( array(
        'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'  => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total'   => $products->max_num_pages,
        'mid_size' => 1,
      ) );
      ?>

    </div>

    <div data-form-sbm-load-triger class="ps-product__columns">

      <?php while ($products->have_posts()) : $products->the_post(); ?>

        <div class="ps-product__column">
          <div class="ps-shoe mb-30">
            <div class="ps-shoe__thumbnail">

              <?php
              /**
               * Bage - NEW
               */
              $isNew = get_field('is_new', get_the_ID());
              ?>
              <?php if ($isNew) : ?>
                <div class="ps-badge">
                  <span>New</span>
                </div>
              <?php endif; ?>

              <?php
              /**
               * SALE PRISE AND %
               * @var $wcProduct WC_Product
               */

              $wcProduct = wc_get_product(get_the_ID());

              if ($wcProduct instanceof WC_Product_Variable) {
                $price = $wcProduct->get_price();
                $price_sale = '';
              } else {
                $price = get_post_meta(get_the_ID(), '_regular_price', true);
                $price_sale = get_post_meta(get_the_ID(), '_sale_price', true);
              }
              ?>
              <?php if ($price_sale !== '') : ?>
                <div class="ps-badge ps-badge--sale <?php echo $isNew ? 'ps-badge--2nd':''; ?>">
                  <span>-<?= ceil((($price - $price_sale) / $price) * 100) ?>%</span>
                </div>
              <?php endif; ?>

              <?php
              $userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id():get_current_user_id();
              $useIdFromUnloggedUser = get_current_user_id() == 0 ? true:false;
              ?>
              <div class="ps-shoe__favorite" style="display: none;" data-wish-loader>
                <img src="<?php echo PHP_IMG; ?>/loadearth.svg" alt="">
              </div>
              <a class="ps-shoe__favorite <?php echo Alg_WC_Wish_List_Item::is_item_in_wish_list(get_the_ID(), $userID, $useIdFromUnloggedUser) ? ' in-wish':'' ?>"
                 href="#" data-add-to-wish-list-btn="<?php echo get_the_ID(); ?>">
                <i class="ps-icon-heart"></i>
              </a>
              <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'custom_size_800_800'); ?>" alt="">
              <a class="ps-shoe__overlay" href="<?php echo get_permalink(get_the_ID()); ?>"></a>
            </div>
            <div class="ps-shoe__content">
              <div class="ps-shoe__variants">

                <?php if (get_field('gallery', get_the_ID())) : ?>

                  <?php
                  $gallery = get_field('gallery', get_the_ID());
                  ?>

                  <div class="ps-shoe__variant normal">

                    <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'custom_size_800_800'); ?>" alt="">

                    <?php foreach ($gallery as $image) : ?>

                      <img src="<?= $image['sizes']['custom_size_800_800']; ?>" alt="">

                    <?php endforeach; ?>

                  </div>

                <?php endif; ?>

              </div>
              <div class="ps-shoe__detail"><a class="ps-shoe__name" href="<?php echo get_permalink(get_the_ID()); ?>"><?= get_the_title(); ?></a>
                <!--                                                    <p class="ps-shoe__categories">-->
                <!--                                                        <a href="#">Men shoes</a>,-->
                <!--                                                        <a href="#"> Nike</a>,-->
                <!--                                                        <a href="#"> Jordan</a>-->
                <!--                                                    </p>-->
                <span class="ps-shoe__price">
                                                        <?php if ($price_sale !== '') : ?>
                                                          <del><?= $price ?> грн.</del> <?= $price_sale ?> грн.
                                                        <?php else: ?>
                                                          <del></del><?= $price ?> грн.
                                                        <?php endif; ?>
                                                    </span>
              </div>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

      <?php if (!$products->have_posts()) : ?>
      <p>Пока таких товаров нету. Но мы рисуем :)</p>
        <img style="display: block; max-width: 558px;width: 100%;" src="<?php echo PHP_IMG . '/no-product.png' ?>">
      <?php endif; ?>

    </div>
    <div data-form-sbm-load-triger class="ps-product-action">

      <?php
      $big = 999999999;
      echo paginate_links_custom( array(
        'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format'  => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total'   => $products->max_num_pages,
        'mid_size' => 1,
      ) );
      ?>

    </div>
  </div>
  <div class="ps-sidebar" data-mh="product-listing">

    <div data-block-clics data-form-sbm-load-triger style="display: none;"></div>

    <?php echo render('service/shop/components/category-filter.php', [
      'categories' => $filterFormView->children['categories'],
    ]) ?>

    <?php echo render('service/shop/components/price-slider-filter.php', [
      'priceMin' => $filterFormView->children['priceMin'],
      'priceMax' => $filterFormView->children['priceMax'],
    ]) ?>

    <?php echo render('service/shop/components/tint-filter.php', [
      'tint' => $filterFormView->children['tint'],
    ]) ?>

    <div class="ps-sticky desktop">

      <?php echo render('service/shop/components/size-filter.php', [
      ]) ?>

      <?php echo render('service/shop/components/color-filter.php', [
        'color' => $filterFormView->children['color'],
      ]) ?>

    </div>
    <!--aside.ps-widget--sidebar-->
    <!--    .ps-widget__header: h3 Ads Banner-->
    <!--    .ps-widget__content-->
    <!--        a(href='product-listing'): img(src="<?php echo HTML_IMG; ?>/offer/sidebar.jpg" alt="")-->
    <!---->
    <!--aside.ps-widget--sidebar-->
    <!--    .ps-widget__header: h3 Best Seller-->
    <!--    .ps-widget__content-->
    <!--        - for (var i = 0; i < 3; i ++)-->
    <!--            .ps-shoe--sidebar-->
    <!--                .ps-shoe__thumbnail-->
    <!--                    a(href='#')-->
    <!--                    img(src="<?php echo HTML_IMG; ?>/shoe/sidebar/"+(i+1)+".jpg" alt="")-->
    <!--                .ps-shoe__content-->
    <!--                    if i == 1-->
    <!--                        a(href='#').ps-shoe__title Nike Flight Bonafide-->
    <!--                    else if i == 2-->
    <!--                        a(href='#').ps-shoe__title Nike Sock Dart QS-->
    <!--                    else-->
    <!--                        a(href='#').ps-shoe__title Men's Sky-->
    <!--                    p <del> £253.00</del> £152.00-->
    <!--                    a(href='#').ps-btn PURCHASE-->
  </div>
</form>

<?php get_footer(); ?>

