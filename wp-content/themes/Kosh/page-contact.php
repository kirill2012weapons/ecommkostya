<?php
/**
 * Template Name: Contact Page
 */
?>

<?php get_header(); ?>

<div class="ps-contact ps-section pb-80">
  <div class="ps-container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
        <div class="ps-section__header mb-50">
          <h2 class="ps-section__title" data-mask="Контакты">- Контакты</h2>

          <?php echo render('contact/contact.php'); ?>

        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="margin-top: 27px;">
        <div class="ps-section__content">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
              <div class="ps-contact__block" data-mh="contact-block" style="height: 262px;">
                <header>
                  <h3>Киев</h3>
                </header>
                <footer>
                  <p><i class="fa fa-map-marker"></i> 19C Trolley Square  Wilmington, DE 19806</p>
                  <p><i class="fa fa-envelope-o"></i><a href="mailto@supportShoes@shoes.net">supportShoes@shoes.net</a></p>
                  <p><i class="fa fa-phone"></i> ( +84 ) 9892 2324  -  9401 123 003</p>
                </footer>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
              <div class="ps-contact__block" data-mh="contact-block" style="height: 288px;">
                <header>
                  <h3>Харьков</h3>
                </header>
                <footer>
                  <p><i class="fa fa-map-marker"></i> 19C Trolley Square  Wilmington, DE 19806</p>
                  <p><i class="fa fa-envelope-o"></i><a href="mailto@supportShoes@shoes.net">supportShoes@shoes.net</a></p>
                  <p><i class="fa fa-phone"></i> ( +84 ) 9892 2324  -  9401 123 003</p>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
