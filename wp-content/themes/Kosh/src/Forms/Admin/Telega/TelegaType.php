<?php

namespace KoshTheme\Forms\Admin\Telega;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class TelegaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('urlWebHook', TextType::class, [
            ])
            ->add('botName', TextType::class, [
            ])
            ->add('botApiKey', TextType::class, [
            ])
            ->add('sales', TextType::class, [
            ])
            ->add('managers', TextType::class, [
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TelegaModel::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'telega_form';
    }

}