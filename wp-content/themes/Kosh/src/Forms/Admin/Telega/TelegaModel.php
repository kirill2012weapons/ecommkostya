<?php

namespace KoshTheme\Forms\Admin\Telega;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class TelegaModel
{

  private $urlWebHook;

  private $botName;

  private $botApiKey;

  private $isConnected;

  private $sales;

  private $managers;

  public function __construct()
  {
    $this->urlWebHook = get_option('telega_bot_urlWebHook');
    $this->botName = get_option('telega_bot_botName');
    $this->botApiKey = get_option('telega_bot_botApiKey');
    $this->isConnected = get_option('telega_bot_isConnected');
    $this->sales = get_option('telega_bot_sales');
    $this->managers = get_option('telega_bot_managers');
  }

  /**
   * @return mixed
   */
  public function getSales()
  {
    return $this->sales;
  }

  /**
   * @param mixed $sales
   */
  public function setSales($sales)
  {
    if ( get_option( 'telega_bot_sales' ) != $sales ) {
      update_option( 'telega_bot_sales', $sales );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_sales', $sales, $deprecated, $autoload );
    }
    $this->managers = $sales;
  }

  /**
   * @return mixed
   */
  public function getManagers()
  {
    return $this->managers;
  }

  /**
   * @param mixed $managers
   */
  public function setManagers($managers)
  {
    if ( get_option( 'telega_bot_managers' ) != $managers ) {
      update_option( 'telega_bot_managers', $managers );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_managers', $managers, $deprecated, $autoload );
    }
    $this->managers = $managers;
  }

  /**
   * @return mixed
   */
  public function getUrlWebHook()
  {
    return $this->urlWebHook;
  }

  /**
   * @param mixed $urlWebHook
   */
  public function setUrlWebHook($urlWebHook)
  {
    if ( get_option( 'telega_bot_urlWebHook' ) != $urlWebHook ) {
      update_option( 'telega_bot_urlWebHook', $urlWebHook );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_urlWebHook', $urlWebHook, $deprecated, $autoload );
    }
    $this->urlWebHook = $urlWebHook;
  }

  /**
   * @return mixed
   */
  public function getBotName()
  {
    return $this->botName;
  }

  /**
   * @param mixed $botName
   */
  public function setBotName($botName)
  {
    if ( get_option( 'telega_bot_botName' ) != $botName ) {
      update_option( 'telega_bot_botName', $botName );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_botName', $botName, $deprecated, $autoload );
    }

    $this->botName = $botName;
  }

  /**
   * @return mixed
   */
  public function getBotApiKey()
  {
    return $this->botApiKey;
  }

  /**
   * @param mixed $botApiKey
   */
  public function setBotApiKey($botApiKey)
  {
    if ( get_option( 'telega_bot_botApiKey' ) != $botApiKey ) {
      update_option( 'telega_bot_botApiKey', $botApiKey );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_botApiKey', $botApiKey, $deprecated, $autoload );
    }

    $this->botApiKey = $botApiKey;
  }

  /**
   * @return mixed
   */
  public function getIsConnected()
  {
    return $this->isConnected;
  }

  /**
   * @param mixed $isConnected
   */
  public function setIsConnected($isConnected)
  {
    if ( get_option( 'telega_bot_isConnected' ) != $isConnected ) {
      update_option( 'telega_bot_isConnected', $isConnected );
    }
    else {
      $deprecated = '';
      $autoload = 'no';
      add_option( 'telega_bot_isConnected', $isConnected, $deprecated, $autoload );
    }

    $this->botApiKey = $isConnected;
  }

}