<?php

namespace KoshTheme\Forms\User\Register;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class RegisterModel
{

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @AcmeAssert\ContainsUserNickNameExists(
   *     message="Пользователь с таким никнеймом уже существует"
   * )
   * @Assert\Regex(
   *   message="Только английские символы, цифры от 0 до 9, знаки '-', '_'. Максимальная длинна 15 символов, минимальная 4",
   *   pattern="/^[A-Za-z]{1}[A-Za-z0-9\-\_]{2,13}[A-Za-z0-9]{1}$/"
   * )
   */
  private $nickName;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 10 символов.",
   *   pattern="/^([А-Яа-я\s\-]){2,9}[а-я]{1}$/u"
   * )
   */
  private $name;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 15 символов.",
   *   pattern="/^([А-Яа-яЁё\s\-]){2,13}[а-я]{1}$/u"
   * )
   */
  private $surname;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Email(
   *     message="Введите email адресс"
   * )
   * @AcmeAssert\ContainsUserEmailIsExists(
   *     message="Пользователь с такой почтой уже существует"
   * )
   */
  private $email;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Состоит из '+380' и вашего номера. Только цифры.",
   *   pattern="/^\+380(50|66|95|67|68|96|99|97|98|63|93|73|91|92|94)[0-9]{7,7}$/u"
   * )
   */
  private $phone;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Минимум 4 символа, содержит 1 букву и цифру. Только английские символы.",
   *   pattern="/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/"
   * )
   */
  private $password;

  /**
   * @return mixed
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone): void
  {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getNickName()
  {
    return $this->nickName;
  }

  /**
   * @param mixed $nickName
   */
  public function setNickName($nickName)
  {
    $this->nickName = $nickName;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getSurname()
  {
    return $this->surname;
  }

  /**
   * @param mixed $surname
   */
  public function setSurname($surname)
  {
    $this->surname = $surname;
  }

  /**
   * @return mixed
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param mixed $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * @return mixed
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param mixed $password
   */
  public function setPassword($password)
  {
    $this->password = $password;
  }

}