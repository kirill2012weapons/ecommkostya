<?php

namespace KoshTheme\Forms\User\Checkout;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class CheckoutFindOrderType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('orderId', TextType::class, [
      ])
      ->add('orderHash', TextType::class, [
      ])
    ;

  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults([
      'data_class' => CheckoutFindOrderModel::class,
    ]);
  }

  public function getBlockPrefix() {
    return 'checkout_find_form';
  }

}