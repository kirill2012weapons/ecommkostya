<?php

namespace KoshTheme\Forms\User\Checkout;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class CheckoutModel
{

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $firstName;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $secondName;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   * @Assert\Email(
   *   message="Введите почтовый адресс"
   * )
   */
  private $emailAddress;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   * @Assert\Regex(
   *   message="Состоит из '+380' и вашего номера. Только цифры.",
   *   pattern="/^\+380(50|66|95|67|68|96|99|97|98|63|93|73|91|92|94)[0-9]{7,7}$/u"
   * )
   */
  private $phone;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $shippingCountry;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $shippingAddress1;

  private $additionInformation;

  /**
   * @Assert\NotBlank(
   *   message="Обязательное поле."
   * )
   */
  private $paymentMethod;

  /**
   * @return mixed
   */
  public function getPaymentMethod() {
    return $this->paymentMethod;
  }

  /**
   * @param mixed $paymentMethod
   */
  public function setPaymentMethod($paymentMethod) {
    $this->paymentMethod = $paymentMethod;
  }

  /**
   * @return mixed
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @param mixed $firstName
   */
  public function setFirstName($firstName) {
    $this->firstName = $firstName;
  }

  /**
   * @return mixed
   */
  public function getSecondName() {
    return $this->secondName;
  }

  /**
   * @param mixed $secondName
   */
  public function setSecondName($secondName) {
    $this->secondName = $secondName;
  }

  /**
   * @return mixed
   */
  public function getEmailAddress() {
    return $this->emailAddress;
  }

  /**
   * @param mixed $emailAddress
   */
  public function setEmailAddress($emailAddress) {
    $this->emailAddress = $emailAddress;
  }

  /**
   * @return mixed
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone) {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getShippingCountry() {
    return $this->shippingCountry;
  }

  /**
   * @param mixed $shippingCountry
   */
  public function setShippingCountry($shippingCountry) {
    $this->shippingCountry = $shippingCountry;
  }

  /**
   * @return mixed
   */
  public function getShippingAddress1() {
    return $this->shippingAddress1;
  }

  /**
   * @param mixed $shippingAddress1
   */
  public function setShippingAddress1($shippingAddress1) {
    $this->shippingAddress1 = $shippingAddress1;
  }

  /**
   * @return mixed
   */
  public function getAdditionInformation() {
    return $this->additionInformation;
  }

  /**
   * @param mixed $additionInformation
   */
  public function setAdditionInformation($additionInformation) {
    $this->additionInformation = $additionInformation;
  }

}