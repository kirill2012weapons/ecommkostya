<?php

namespace KoshTheme\Forms\User\Checkout;


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;

class CheckoutType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $nvpoch = container()->get('factory.novaya_pochta');
    $cities = $nvpoch->getCities();

    $choices = [];
    if (isset($cities['data'])) {
      foreach ($cities['data'] as $city) {
        $choices[$city['DescriptionRu']] = $city['Ref'];
      }
    }

    $builder
      ->add('firstName', TextType::class, [
      ])
      ->add('secondName', TextType::class, [
      ])
      ->add('emailAddress', TextType::class, [
      ])
      ->add('phone', TextType::class, [
      ])
      ->add('shippingCountry', ChoiceType::class, [
        'choices' => $choices,
      ])
      ->add('shippingAddress1', TextType::class, [
      ])
      ->add('additionInformation', TextType::class, [
      ])
      ->add('paymentMethod', ChoiceType::class, [
        'choices' => [
          'Новая почта' => 'nal_pay',
          'Оплата карточкой' => 'card_pay'
        ],
      ])
    ;

    $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
      $checkout     = $event->getData();
      $form         = $event->getForm();

      if (!$checkout) {
        return;
      }

      /**
       * TODO: ADD LOGIC TO CREATE AREA AFTER WRONG INPUTS
       */

    });

  }

  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults([
      'data_class' => CheckoutModel::class,
    ]);
  }

  public function getBlockPrefix() {
    return 'checkout_form';
  }

}