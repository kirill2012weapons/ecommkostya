<?php

namespace KoshTheme\Forms\User\Billing;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class BillingModel
{

  private $billingFirstName;

  private $billingSecondName;

  private $billingCompany;

  private $billingAddress1;

  private $billingAddress2;

  private $billingCity;

  private $billingPostCode;

  private $billingCountry;

  private $billingState;

  /**
   * @return mixed
   */
  public function getBillingFirstName() {
    return $this->billingFirstName;
  }

  /**
   * @param mixed $billingFirstName
   */
  public function setBillingFirstName($billingFirstName) {
    $this->billingFirstName = $billingFirstName;
  }

  /**
   * @return mixed
   */
  public function getBillingSecondName() {
    return $this->billingSecondName;
  }

  /**
   * @param mixed $billingSecondName
   */
  public function setBillingSecondName($billingSecondName) {
    $this->billingSecondName = $billingSecondName;
  }

  /**
   * @return mixed
   */
  public function getBillingCompany() {
    return $this->billingCompany;
  }

  /**
   * @param mixed $billingCompany
   */
  public function setBillingCompany($billingCompany) {
    $this->billingCompany = $billingCompany;
  }

  /**
   * @return mixed
   */
  public function getBillingAddress1() {
    return $this->billingAddress1;
  }

  /**
   * @param mixed $billingAddress1
   */
  public function setBillingAddress1($billingAddress1) {
    $this->billingAddress1 = $billingAddress1;
  }

  /**
   * @return mixed
   */
  public function getBillingAddress2() {
    return $this->billingAddress2;
  }

  /**
   * @param mixed $billingAddress2
   */
  public function setBillingAddress2($billingAddress2) {
    $this->billingAddress2 = $billingAddress2;
  }

  /**
   * @return mixed
   */
  public function getBillingCity() {
    return $this->billingCity;
  }

  /**
   * @param mixed $billingCity
   */
  public function setBillingCity($billingCity) {
    $this->billingCity = $billingCity;
  }

  /**
   * @return mixed
   */
  public function getBillingPostCode() {
    return $this->billingPostCode;
  }

  /**
   * @param mixed $billingPostCode
   */
  public function setBillingPostCode($billingPostCode) {
    $this->billingPostCode = $billingPostCode;
  }

  /**
   * @return mixed
   */
  public function getBillingCountry() {
    return $this->billingCountry;
  }

  /**
   * @param mixed $billingCountry
   */
  public function setBillingCountry($billingCountry) {
    $this->billingCountry = $billingCountry;
  }

  /**
   * @return mixed
   */
  public function getBillingState() {
    return $this->billingState;
  }

  /**
   * @param mixed $billingState
   */
  public function setBillingState($billingState) {
    $this->billingState = $billingState;
  }


}