<?php

namespace KoshTheme\Forms\User\Information;


use KoshTheme\Validator\Constraints\ContainsUserEmailIsExists;
use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class InformationModel
{

  /**
   * @Assert\Image(
   *   uploadErrorMessage="Загрузка не удалась :(",
   *   maxSizeMessage="Максимальный размер - 5МБ",
   *   maxSize="5M"
   * )
   */
  private $logoUploadFile;

  private $logo;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Состоит из '+380' и вашего номера. Только цифры.",
   *   pattern="/^\+380(50|66|95|67|68|96|99|97|98|63|93|73|91|92|94)[0-9]{7,7}$/u"
   * )
   */
  private $phone;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @AcmeAssert\ContainsUserNickNameExists(
   *     message="Пользователь с таким никнеймом уже существует"
   * )
   * @Assert\Regex(
   *   message="Только английские символы, цифры от 0 до 9, знаки '-', '_'. Максимальная длинна 15 символов, минимальная 4",
   *   pattern="/^[A-Za-z]{1}[A-Za-z0-9\-\_]{2,13}[A-Za-z0-9]{1}$/"
   * )
   */
  private $nickName;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 10 символов.",
   *   pattern="/^([А-Яа-я\s\-]){2,9}[а-я]{1}$/u"
   * )
   */
  private $name;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 15 символов.",
   *   pattern="/^([А-Яа-яЁё\s\-]){2,13}[а-я]{1}$/u"
   * )
   */
  private $secondName;

  private $description;

  /**
   * @return mixed
   */
  public function getLogoUploadFile() {
    return $this->logoUploadFile;
  }

  /**
   * @param mixed $logoUploadFile
   */
  public function setLogoUploadFile($logoUploadFile) {
    $this->logoUploadFile = $logoUploadFile;
  }

  /**
   * @return mixed
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * @param mixed $phone
   */
  public function setPhone($phone): void
  {
    $this->phone = $phone;
  }

  /**
   * @return mixed
   */
  public function getLogo() {
    return $this->logo;
  }

  /**
   * @param mixed $logo
   */
  public function setLogo($logo) {
    $this->logo = $logo;
  }

  /**
   * @return mixed
   */
  public function getNickName() {
    return $this->nickName;
  }

  /**
   * @param mixed $nickName
   */
  public function setNickName($nickName) {
    $this->nickName = $nickName;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getSecondName() {
    return $this->secondName;
  }

  /**
   * @param mixed $secondName
   */
  public function setSecondName($secondName) {
    $this->secondName = $secondName;
  }

  /**
   * @return mixed
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description) {
    $this->description = strip_tags($description);
  }

}