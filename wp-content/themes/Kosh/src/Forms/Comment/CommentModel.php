<?php

namespace KoshTheme\Forms\Comment;


use Symfony\Component\Validator\Constraints as Assert;
use KoshTheme\Validator\Constraints as AcmeAssert;

class CommentModel
{
  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Хули сюда лезешь и меняешь кек",
   *   pattern="/^[0-9]+$/u"
   * )
   */
  private $postId;

  private $comment;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 10 символов.",
   *   pattern="/^([А-Яа-я\s\-]){2,9}[а-я]{1}$/u"
   * )
   */
  private $name;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Regex(
   *   message="Только русские буквы. Максимальная длинна 15 символов.",
   *   pattern="/^([А-Яа-яЁё\s\-]){2,13}[а-я]{1}$/u"
   * )
   */
  private $surname;

  /**
   * @Assert\NotBlank(
   *     message="Обязательно поле"
   * )
   * @Assert\Email(
   *     message="Введите email адресс"
   * )
   */
  private $email;

  /**
   * @return mixed
   */
  public function getPostId() {
    return $this->postId;
  }

  /**
   * @param mixed $postId
   */
  public function setPostId($postId) {
    $this->postId = $postId;
  }

  /**
   * @return mixed
   */
  public function getComment() {
    return $this->comment;
  }

  /**
   * @param mixed $comment
   */
  public function setComment($comment) {
    $this->comment = $comment;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getSurname()
  {
    return $this->surname;
  }

  /**
   * @param mixed $surname
   */
  public function setSurname($surname)
  {
    $this->surname = $surname;
  }

  /**
   * @return mixed
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param mixed $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

}