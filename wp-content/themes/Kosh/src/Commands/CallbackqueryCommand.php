<?php

namespace Longman\TelegramBot\Commands\SystemCommands;


use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

class CallbackqueryCommand extends SystemCommand
{
  protected $name = 'callbackquery';
  protected $description = 'Reply to callback query';
  protected $version = '1.0.0';

  public function execute()
  {

    $update         = $this->getUpdate();
    $callback_query = $update->getCallbackQuery();
    $callback_data  = $callback_query->getData();

    if (strpos($callback_data, 'newordercallcomplate_') !== false) {

      $order = substr($callback_data, strlen('newordercallcomplate_'));
      container()->get('telega.order')->editMessageThatCallWas(
        $callback_query->getMessage()->getChat()->getId(),
        $callback_query->getMessage()->getMessageId(),
        $order
      );

    } else if (strpos($callback_data, 'canceledorder_') !== false) {

      $order = substr($callback_data, strlen('canceledorder_'));
      container()->get('telega.order')->canselOrderStatus(
        $callback_query->getMessage()->getChat()->getId(),
        $callback_query->getMessage()->getMessageId(),
        $order
      );

    } else if (strpos($callback_data, 'sendorder_') !== false) {

      $order = substr($callback_data, strlen('sendorder_'));
      container()->get('telega.order')->sendOrderToManager(
        $callback_query->getMessage()->getChat()->getId(),
        $callback_query->getMessage()->getMessageId(),
        $order
      );

    } else if (strpos($callback_data, 'sended_') !== false) {

      $order = substr($callback_data, strlen('sended_'));
      container()->get('telega.order')->finOrderSend(
        $callback_query->getMessage()->getChat()->getId(),
        $callback_query->getMessage()->getMessageId(),
        $order
      );

    } else {
      return Request::emptyResponse();
    }

  }
}