<?php

namespace KoshTheme\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsOrderExists extends Constraint
{
  public $message = 'Проверьте информацию о заказе еще раз.';

  public function getTargets() {
    return self::CLASS_CONSTRAINT;
  }

}