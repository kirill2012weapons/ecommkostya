<?php
remove_filter('authenticate', 'wp_authenticate_username_password', 20);
add_filter('authenticate', function ($user, $email, $password) {

  if (empty($email) || empty ($password)) {
    $error = new WP_Error();

    if (empty($email)) {
      $error->add('empty_username', __('Поле email обязательное.'));
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { //Invalid Email
      $error->add('invalid_username', __('Поле email не валидное.'));
    }

    if (empty($password)) {
      $error->add('empty_password', __('Пароль обязательный.'));
    }

    return $error;
  }

  $user = get_user_by('email', $email);

  if (!$user) {
    $error = new WP_Error();
    $error->add('invalid', __('Попробуйте ввести свой имейл и пароль.'));
    return $error;
  } else {
    if (!wp_check_password($password, $user->user_pass, $user->ID)) { //bad password
      $error = new WP_Error();
      $error->add('invalid', __('Попробуйте ввести свой имейл и пароль.'));
      return $error;
    } else {
      return $user; //passed
    }
  }
}, 20, 3);


add_action('init', 'login_url');
function login_url () {

  if (!isset($_POST['login_form'])) return;

  $user_data['user_login']      = $_POST['login_form']['email'];
  $user_data['user_password']   = $_POST['login_form']['password'];
  $user_data['remember']        = false;
  $user = wp_signon($user_data, false);

  if (!is_wp_error($user)) {
    wp_set_current_user( $user->ID, $user->user_login );
    wp_set_auth_cookie($user->ID);
    do_action( 'wp_login', $user->user_login, $user );
    wp_redirect( home_url() );
    exit;
  }
}