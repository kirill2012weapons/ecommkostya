<?php

add_action('wp_enqueue_scripts', 'myajax_data', 99);
function myajax_data() {
  wp_localize_script('compiledfirstpartjsminjs', 'myajax',
    array(
      'url' => admin_url('admin-ajax.php'),
      'theme_root' => get_stylesheet_directory_uri()
    )
  );
}

function ajaxRender($data = [], $code = 200, $isSuccess = true, $error = '') {
  echo json_encode([
    'code' => $code,
    'success' => $isSuccess,
    'data' => $data,
    'error' => $error
  ]);
  wp_die();
}

/**
 * AJAX ADD TO CARD ITEM
 * ROUTE - add_to_card_item
 */
add_action('wp_ajax_add_to_card_item', 'addToCardItem');
add_action('wp_ajax_nopriv_add_to_card_item', 'addToCardItem');
function addToCardItem() {

  if (!isset($_POST['item_id']) || !isset($_POST['item_count'])) ajaxRender([], 200, false);

  try {
    WC()->cart->add_to_cart($_POST['item_id'], $_POST['item_count']);
  } catch (Exception $exception) {
    ajaxRender([], 200, false, $exception->getMessage());
  }

  $data = [
    'count' => WC()->cart->get_cart_contents_count(),
    'total_sum' => WC()->cart->get_cart_total(),
    'items_html' => render('mini-card/item-template.php'),
  ];

  ajaxRender($data, 200, true);

}


/**
 * AJAX REMOVE FROM CARD ITEM
 * ROUTE - remove_from_card_item
 */
add_action('wp_ajax_remove_from_card_item', 'removeFromCardItem');
add_action('wp_ajax_nopriv_remove_from_card_item', 'removeFromCardItem');
function removeFromCardItem() {

  if (!isset($_POST['item_id'])) ajaxRender([], 200, false);

  $product = wc_get_product($_POST['item_id']);

  if ($product instanceof WC_Product_Variation) {
    $cartId = WC()->cart->generate_cart_id($product->get_parent_id(), $product->get_id());
    $cartItemKey = WC()->cart->find_product_in_cart($cartId);
  } else {
    $cartId = WC()->cart->generate_cart_id($_POST['item_id']);
    $cartItemKey = WC()->cart->find_product_in_cart($cartId);
  }

  try {
    WC()->cart->remove_cart_item($cartItemKey);
  } catch (Exception $exception) {
    ajaxRender([], 200, false, $exception->getMessage());
  }

  $data = [
    'count' => WC()->cart->get_cart_contents_count(),
    'total_sum' => WC()->cart->get_cart_total(),
    'items_html' => render('mini-card/item-template.php'),
    '$cartItemKey' => $cartItemKey
  ];

  ajaxRender($data, 200, true);
}

/**
 * AJAX REMOVE FROM CARD ITEM
 * ROUTE - add_to_card_item
 */
add_action('wp_ajax_get_all_parts_by_ref', 'getAllPartsInAreaByRef');
add_action('wp_ajax_nopriv_get_all_parts_by_ref', 'getAllPartsInAreaByRef');
function getAllPartsInAreaByRef() {

  if (!isset($_POST['ref'])) ajaxRender([], 200, false);

  try {
    $nvpoch = container()->get('factory.novaya_pochta');
    $part = $nvpoch->getWarehouses($_POST['ref']);
    if ($part['success'] == false) ajaxRender([], 200, false, 'No data.');
  } catch (Exception $exception) {
    ajaxRender([], 200, false, $exception->getMessage());
  }

  foreach ($part['data'] as $part) {
    $data[] = [
      'ref' => $part['Ref'],
      'DescriptionRu' => $part['DescriptionRu'],
    ];
  }

  ajaxRender($data, 200, true);
}

/**
 * AJAX REMOVE FROM CARD ITEM
 * ROUTE - get_items_from_card_popup
 */
add_action('wp_ajax_get_items_from_card_popup', 'getItemsFromCardPopup');
add_action('wp_ajax_nopriv_get_items_from_card_popup', 'getItemsFromCardPopup');
function getItemsFromCardPopup() {

  $data = render('popup/cart-content.php');

  ajaxRender($data, 200, true);

}

/**
 * AJAX REMOVE FROM CARD ITEM AND ADD
 * ROUTE - change_items_from_card_popup
 */
add_action('wp_ajax_change_items_from_card_popup', 'changeCountItemsFromCardPopup');
add_action('wp_ajax_nopriv_change_items_from_card_popup', 'changeCountItemsFromCardPopup');
function changeCountItemsFromCardPopup() {

  if (!isset($_POST['item_id']) || !isset($_POST['item_count'])) ajaxRender([], 200, false);


  try {

    $product = wc_get_product($_POST['item_id']);

    if ($product instanceof WC_Product_Variation) {
      $cartId = WC()->cart->generate_cart_id($product->get_parent_id(), $product->get_id());
      $cartItemKey = WC()->cart->find_product_in_cart($cartId);
    } else {
      $cartId = WC()->cart->generate_cart_id($_POST['item_id']);
      $cartItemKey = WC()->cart->find_product_in_cart($cartId);
    }


    WC()->cart->set_quantity($cartItemKey, $_POST['item_count']);
  } catch (Exception $exception) {
    ajaxRender([], 200, false, $exception->getMessage());
  }

  ajaxRender([], 200, true);
}

/**
 * AJAX REMOVE FROM CARD ITEM
 * ROUTE - get_items_from_card_mini
 */
add_action('wp_ajax_get_items_from_card_mini', 'getItemsFromCardMini');
add_action('wp_ajax_nopriv_get_items_from_card_mini', 'getItemsFromCardMini');
function getItemsFromCardMini() {

  $data = render('mini-card/card-base.php');

  ajaxRender($data, 200, true);
}

/**
 * AJAX REMOVE FROM CARD ITEM
 * ROUTE - update_checkout_form
 */
add_action('wp_ajax_update_checkout_form', 'updateCheckoutForm');
add_action('wp_ajax_nopriv_update_checkout_form', 'updateCheckoutForm');
function updateCheckoutForm() {

  /**
   * @var $formFactory \Symfony\Component\Form\FormFactory
   */
  $formFactory = container()->get('factory.form.annotation_validator');
  $checkoutModel = new \KoshTheme\Forms\User\Checkout\CheckoutModel();
  $checkoutForm = $formFactory
    ->createBuilder(\KoshTheme\Forms\User\Checkout\CheckoutType::class, $checkoutModel)
    ->getForm();

  $items = WC()->cart->get_cart();

  $checkoutFormView = $checkoutForm->createView();

  $data = render('checkout/base_wrap.php', [
    'checkoutFormView' => $checkoutFormView,
    'items' => $items
  ]);

  ajaxRender($data, 200, true);
}

/**
 * AJAX ADD WISH
 * ROUTE - add_wish_item
 */
add_action('wp_ajax_add_wish_item', 'addWishItem');
add_action('wp_ajax_nopriv_add_wish_item', 'addWishItem');
function addWishItem() {

  $data = [];

  if ( !isset($_POST['item_id']) ) ajaxRender([], 200, false);

  $userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id() : get_current_user_id();
  $useIdFromUnloggedUser = get_current_user_id() == 0 ? true : false;

  $data = Alg_WC_Wish_List_Item::toggle_item_from_wish_list($_POST['item_id'], $userID, $useIdFromUnloggedUser);

  ajaxRender($data, 200, true);

}
