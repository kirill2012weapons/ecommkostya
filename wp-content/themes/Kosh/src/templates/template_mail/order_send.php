<?php
/**
 * @var $order WC_Order
 */
?>

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
       style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; max-width: 600px !important;">
  <tbody>
  <tr>
    <td valign="top" id="templatePreheader"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0px;"></td>
  </tr>
  <tr>
    <td valign="top" id="templateHeader"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="CodeBlock"
             style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner"
              style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"></td>
        </tr>
        </tbody>
        <tbody>
        <tr>
          <td class="mobile-hide mobile-no-padding text-padding-left" width="450" height="110" align="left"
              style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding-left: 58px; mso-line-height-rule: exactly;">
            <a href="<?php echo $siteUrl; ?>" target="_blank"
               style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #111111; mso-line-height-rule: exactly; font-weight: normal; text-decoration: underline;"><img
                  src="<?php echo $siteUrlImg; ?>" alt="Themes Email" width="120" height="20"
                  style="border: 0; display: inline-block; font-size: 13px; font-weight: bold; height: auto; outline: none; text-decoration: none; text-transform: capitalize; vertical-align: middle; margin-right: 10px; -ms-interpolation-mode: bicubic;"
                  data-pagespeed-url-hash="2761749788" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" id="templateBody"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 2px solid #EAEAEA; padding-top: 0; padding-bottom: 9px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock"
             style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner" style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                   class="TextContentContainer">
              <tbody>
              <tr>
                <td valign="top" class="TextContent"
                    style="padding: 9px 18px; text-align: left; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #222222; font-family: Helvetica; font-size: 13px; line-height: 150%;">
                  <table border="0" cellpadding="0" cellspacing="0" max-width="564" width="100%"
                         style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                    <tbody>
                    <tr>
                      <td id="emailcontent"
                          style="padding-left: 7%; padding-right: 7%; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

                        <!--  Here Goes Content: Start  -->
                        <p style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #222222; font-family: Helvetica; font-size: 13px; line-height: 150%; text-align: left;">
                        Ваш заказ оформлен. В ближайшее время мы вам перезвоним.
                        </p>


                        <h2 style="color: #656565; display: block; font-family: Helvetica; font-size: 18px; font-weight: bold; line-height: 125%; margin: 15px 0 10px 0; text-align: left; font-style: normal; letter-spacing: normal;">
                          Заказ # <?php echo $order->get_id(); ?>
                        </h2>
                        <h2 style="color: #656565; display: block; font-family: Helvetica; font-size: 18px; font-weight: bold; line-height: 125%; margin: 15px 0 10px 0; text-align: left; font-style: normal; letter-spacing: normal;">
                          Уникальный номер заказа - <?php echo $order->get_order_key(); ?>
                        </h2>

                        <div style="margin-bottom: 40px;">
                          <table class="td" cellspacing="0" cellpadding="6"
                                 style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; vertical-align: middle;">
                            <thead>
                            <tr>
                              <th class="td" scope="col"
                                  style="text-align: left; color: #4e4e4e; vertical-align: middle; border-bottom: 1px solid #ebebeb;">Товар
                              </th>
                              <th class="td" scope="col"
                                  style="text-align: left; color: #4e4e4e; vertical-align: middle; border-bottom: 1px solid #ebebeb;">Кол.
                              </th>
                              <th class="td" scope="col"
                                  style="text-align: left; color: #4e4e4e; vertical-align: middle; border-bottom: 1px solid #ebebeb;">Цена
                              </th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($order->get_items() as $orderItem) : ?>
                              <?php
                              /**
                               * @var $orderItem WC_Order_Item_Product
                               */
                              ?>

                              <tr class="order_item">
                              <td class="td"
                                  style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;font-weight: bold;">
                                <img width="64" height="64" src="<?php echo get_the_post_thumbnail_url($orderItem->get_product_id()); ?>"
                                     class="attachment-64x64 size-64x64 wp-post-image" alt=""
                                     style="border: 0; display: inline-block; font-size: 13px; font-weight: bold; height: auto !important; outline: none; text-decoration: none; text-transform: capitalize; vertical-align: middle; margin-right: 10px; -ms-interpolation-mode: bicubic;"
                                     data-pagespeed-url-hash="2002664947" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                <?php echo $orderItem->get_name() ?>
                              </td>
                              <td class="td"
                                  style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                <?php echo $orderItem->get_quantity() ?>
                              </td>
                              <td class="td"
                                  style="text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; border-bottom: 1px solid #ebebeb;">
                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span><?php echo $orderItem->get_total() ?> грн.</span></td>
                            </tr>

                            <?php endforeach; ?>

                            </tbody>
                            <tfoot style="background-color: #fafafa;">
                            <tr>
                              <th class="td" scope="row" colspan="2"
                                  style="text-align: right; border-top-width: 0px; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                Всего:
                              </th>
                              <td class="td"
                                  style="text-align: left; border-top-width: 0px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">
                                  </span><?php echo $order->get_total(); ?> грн.</span>
                              </td>
                            </tr>
                            <tr>
                              <th class="td" scope="row" colspan="2"
                                  style="text-align: right; border-top-width: 0px; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                Доставка:
                              </th>
                              <td class="td"
                                  style="text-align: left; border-top-width: 0px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">
                                  </span>Новой Почтой</span>
                              </td>
                            </tr>
                            <tr>
                              <th class="td" scope="row" colspan="2"
                                  style="text-align: right; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                Способ оплаты:
                              </th>
                              <td class="td"
                                  style="text-align: left; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4e4e4e; vertical-align: middle; padding-top: 8px !important; padding-bottom: 8px !important;">
                                <?php
                                if ($order->get_payment_method() == 'liqpay-webplus') echo 'Оплата картой';
                                if ($order->get_payment_method() == '') echo 'Наложенный платеж';
                                ?>
                              </td>
                            </tr>
                            </tfoot>
                          </table>
                        </div>

                        <table id="addresses" cellspacing="0" cellpadding="0"
                               style="width: 100%; vertical-align: top; margin-bottom: 40px; padding: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                               border="0">
                          <tbody>
                          <tr>
                            <td style="text-align: left; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border: 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                valign="top" width="50%">
                              <h2 style="color: #656565; display: block; font-family: Helvetica; font-size: 18px; font-weight: bold; line-height: 125%; margin: 15px 0 10px 0; text-align: left; font-style: normal; letter-spacing: normal;">
                                Контактная Информация</h2>

                              <address class="address" style="color: #4e4e4e;">
                                <?php echo $order->get_billing_first_name(); ?> <?php echo $order->get_billing_last_name(); ?><br>
                                <?php echo $order->get_shipping_city(); ?><br>
                                <?php echo $order->get_shipping_address_1(); ?><br>
                                <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #222222; font-family: Helvetica; font-size: 13px; line-height: 150%; text-align: left;">
                                  <?php echo $order->get_billing_phone(); ?>
                                </p>
                              </address>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                        <!--  Here Goes Content: End  --><br>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" id="templateFooter"
        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ededed; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="TextBlock"
             style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <tbody class="TextBlockOuter">
        <tr>
          <td valign="top" class="TextBlockInner" style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                   class="TextContentContainer">
              <tbody>
              <tr>
                <td valign="top" class="TextContent"
                    style="padding-top: 9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #656565; font-family: Helvetica; font-size: 12px; line-height: 150%; text-align: center;">
                  <div style="text-align: center;">
                    <br>
                    Дополнительная контактная информация по номеру - <?php echo $contactPhone; ?><br>
                    Email - <?php echo $contactEmail; ?>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>