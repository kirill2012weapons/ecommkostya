<?php
/**
 * @var $order Order
 * @var $orderHash string
 */

?>
<div class="ps-cart-listing ps-table--compare">

  <?php if ($order && $orderHash == $order->get_order_key()) : ?>
    <h3 style="    text-align: center;
    margin-bottom: 20px;">Информация о заказе</h3>
    <p>Спасибо за покупку, в скором времени мы свяжемся с вами.</p>
    <p style="font-weight: 700; color: #2ac27d;">Запомните номер заказа и хэш заказа, что бы отслеживать.</p>
    <table class="table ps-cart__table">
      <tbody>

      <tr>
        <td style="width: 30%;">Заказ #</td>
        <td>
          <?php echo $order->get_id(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Хэш заказа</td>
        <td>
          <?php echo $order->get_order_key(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Имя Фамилия</td>
        <td><span class="price"><?php echo $order->get_billing_first_name(); ?> <?php echo $order->get_billing_last_name(); ?></span></td>
      </tr>

      <tr>
        <td style="width: 30%;">Номер Телефона</td>
        <td>
          <?php echo $order->get_billing_phone(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Сумма покупки</td>
        <td><span class="price"><?php echo $order->get_total(); ?> грн.</span></td>
      </tr>

      <tr>
        <td style="width: 30%;">Статус заказа</td>
        <td>
          <?php echo $order->get_status(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Город</td>
        <td>
          <?php echo $order->get_shipping_city(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Адресс</td>
        <td>
          <?php echo $order->get_shipping_address_1(); ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Статус отправки</td>
        <td>
          <?php
          $status = get_field('o_g_status_shipments', $order->get_id())['label'];
          if (is_null($status)) $status = 'Не отправлен';
          echo $status;
          ?>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Способ оплаты</td>
        <td>
            <span class="status">
              <?php
              if ($order->get_payment_method() == 'liqpay-webplus') echo 'Оплата карточкой';
              if ($order->get_payment_method() == '') echo 'Наложенный платеж';
              ?>
            </span>
        </td>
      </tr>

      <tr>
        <td style="width: 30%;">Товары</td>
        <td>
          <?php foreach ($order->get_items() as $orderItem) : ?>
            <?php
            /**
             * @var $orderItem WC_Order_Item_Product
             */
            ?>
            <div>
              <?php echo $orderItem->get_name() ?> - x<?php echo $orderItem->get_quantity() ?> - <?php echo $orderItem->get_total() ?> грн.
            </div>
          <?php endforeach; ?>
        </td>
      </tr>

      </tbody>
    </table>
  <?php else: ?>
    <div style="margin-bottom: 40px; text-align: center;">
      <h3>Неправильный номер заказа или хэш.</h3><br>
      <p>Найти заказ по номеру и хэшу.</p>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="form-group">
          <a href="<?php echo home_url('/order-finder/'); ?>" class="ps-btn">Поиск<i class="ps-icon-next"></i></a>
        </div>
      </div>
    </div>
  <?php endif; ?>

</div>