<div class="ps-checkout__order">
  <header>
    <h3>ВАШ ЗАКАЗ</h3>
  </header>
  <div class="content">
    <table class="table ps-checkout__products">
      <thead>
      <tr>
        <th class="text-uppercase">Товар</th>
        <th class="text-uppercase">Всего</th>
      </tr>
      </thead>
      <tbody>

      <?php foreach ($items as $item => $values) : ?>

        <?php
        $product = wc_get_product($values['data']->get_id());
        $getProductDetail = wc_get_product($values['product_id']);
        ?>

        <tr>
          <td><?php echo $product->get_title() ?> x<?php echo $values['quantity']; ?></td>
          <td><?= $product->get_price(); ?> грн.</td>
        </tr>

      <?php endforeach; ?>
      <tr>
        <td>Всего стоимость:</td>
        <td><?php echo WC()->cart->get_cart_total(); ?> (грн.)</td>
      </tr>
      </tbody>
    </table>
  </div>
  <footer>

    <h3>Метод оплаты:</h3>
    <div class="form-group cheque">
      <div class="ps-radio">
        <input class="form-control"
               type="radio"
               id="rdo01"
               name="<?php echo $checkoutFormView->children['paymentMethod']->vars['full_name']; ?>"
               value="<?php echo $checkoutFormView->children['paymentMethod']->vars['choices'][0]->value; ?>"
              <?php if ($checkoutFormView->children['paymentMethod']->vars['value'] == $checkoutFormView->children['paymentMethod']->vars['choices'][0]->value) : ?>
                checked
              <?php endif; ?>
        >
        <label for="rdo01">Новая почта</label>
        <p>Наложеный платеж</p>
      </div>
    </div>
    <div class="form-group paypal">
      <div class="ps-radio ps-radio--inline">
        <input class="form-control"
               type="radio"
               name="<?php echo $checkoutFormView->children['paymentMethod']->vars['full_name']; ?>"
               value="<?php echo $checkoutFormView->children['paymentMethod']->vars['choices'][1]->value; ?>"
              <?php if (empty($checkoutFormView->children['paymentMethod']->vars['value'])) : ?>
                checked
              <?php elseif ($checkoutFormView->children['paymentMethod']->vars['value'] == $checkoutFormView->children['paymentMethod']->vars['choices'][1]->value) : ?>
                checked
              <?php endif; ?>
               id="rdo02"
        >
        <label for="rdo02">Оплата карточкой</label>
      </div>
      <button class="ps-btn ps-btn--fullwidth">Заказать<i class="ps-icon-next"></i></button>
    </div>
  </footer>
</div>