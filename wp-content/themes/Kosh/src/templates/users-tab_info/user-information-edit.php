<?php
/**
 * @var $curUser WP_User
 * @var $currUserMeta object
 */

use KoshTheme\Forms\User\Information\InformationModel;
use KoshTheme\Forms\User\Information\InformationType;
use KoshTheme\Forms\User\Billing\BillingModel;
use KoshTheme\Forms\User\Billing\BillingType;

?>

<?php

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

/**
 * +++++++++++++++++++++++++++ INFORMATION MODER START +++++++++++++++++++++++++++
 */
$informationModel = new InformationModel();
$informationModel->setName($curUser->first_name);
$informationModel->setSecondName($curUser->last_name);
$informationModel->setNickName($curUser->nickname);
$informationModel->setDescription($curUser->description);
$informationModel->setPhone($curUser->billing_phone);
if (isset($currUserMeta->wp_user_avatar)) $informationModel->setLogo( $currUserMeta->wp_user_avatar[0] );


$formInformation = $formFactory->createBuilder(InformationType::class, $informationModel)
                    ->getForm();
$formInformation->handleRequest($request);

if ($formInformation->isSubmitted() && $formInformation->isValid()) {

  $userData = [
    'ID'              => $curUser->ID,
    'nickname'        => $informationModel->getNickName(),
    'user_login'        => $informationModel->getNickName(),
    'first_name'      => $informationModel->getName(),
    'last_name'       => $informationModel->getSecondName(),
    'description'     => $informationModel->getDescription(),
  ];

  if ($informationModel->getLogoUploadFile() instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
    $attachmentID = CustomImageUpload::upload('information_form', 'logoUploadFile', 0);

    if ($attachmentID) {
      update_user_meta( $curUser->ID, 'wp_user_avatar', $attachmentID );
    }
  }

  $userId = wp_update_user($userData);
  update_user_meta( $curUser->ID, 'billing_phone',          $informationModel->getPhone() );

  wp_safe_redirect( home_url('/account/') );
  die();

}

$formInformationForm = $formInformation->createView();

/**
 * +++++++++++++++++++++++++++ INFORMATION MODER END +++++++++++++++++++++++++++
 */

/**
 * +++++++++++++++++++++++++++ INFORMATION BILLING MODER START +++++++++++++++++++++++++++
 */

$informationBillingModel = new BillingModel();
$informationBillingModel->setBillingAddress1(     isset($currUserMeta->billing_address_1)     ? $currUserMeta->billing_address_1[0] : '' );
$informationBillingModel->setBillingAddress2(     isset($currUserMeta->billing_address_2)     ? $currUserMeta->billing_address_2[0] : '' );
$informationBillingModel->setBillingCity(         isset($currUserMeta->billing_city)          ? $currUserMeta->billing_city[0] : '' );
$informationBillingModel->setBillingCompany(      isset($currUserMeta->billing_company)       ? $currUserMeta->billing_company[0] : '' );
$informationBillingModel->setBillingCountry(      isset($currUserMeta->billing_country)       ? $currUserMeta->billing_country[0] : '' );
$informationBillingModel->setBillingFirstName(    isset($currUserMeta->billing_first_name)    ? $currUserMeta->billing_first_name[0] : '' );
$informationBillingModel->setBillingPostCode(     isset($currUserMeta->billing_postcode)      ? $currUserMeta->billing_postcode[0] : '' );
$informationBillingModel->setBillingSecondName(   isset($currUserMeta->billing_last_name)     ? $currUserMeta->billing_last_name[0] : '' );
$informationBillingModel->setBillingState(        isset($currUserMeta->billing_state)         ? $currUserMeta->billing_state[0] : '' );

$formBillingInformation = $formFactory->createBuilder(BillingType::class, $informationBillingModel)
  ->getForm();

$formBillingInformation->handleRequest($request);

if ($formBillingInformation->isSubmitted() && $formBillingInformation->isValid()) {

  update_user_meta( $curUser->ID, 'billing_address_1',      $informationBillingModel->getBillingAddress1() );
  update_user_meta( $curUser->ID, 'billing_address_2',      $informationBillingModel->getBillingAddress2() );
  update_user_meta( $curUser->ID, 'billing_city',           $informationBillingModel->getBillingCity() );
  update_user_meta( $curUser->ID, 'billing_company',        $informationBillingModel->getBillingCompany() );
  update_user_meta( $curUser->ID, 'billing_country',        $informationBillingModel->getBillingCountry() );
  update_user_meta( $curUser->ID, 'billing_first_name',     $informationBillingModel->getBillingFirstName() );
  update_user_meta( $curUser->ID, 'billing_postcode',       $informationBillingModel->getBillingPostCode() );
  update_user_meta( $curUser->ID, 'billing_last_name',      $informationBillingModel->getBillingSecondName() );
  update_user_meta( $curUser->ID, 'billing_state',          $informationBillingModel->getBillingState() );

  wp_safe_redirect( home_url('/account/') );
  die();

}

$formBillingInformationForm = $formBillingInformation->createView();

/**
 * +++++++++++++++++++++++++++ INFORMATION BILLING MODER END +++++++++++++++++++++++++++
 */

?>

<p class="mb-20">Профиль <strong><?php echo $curUser->nickname; ?></strong></p>

  <h4>РЕДАКТИРОВАТЬ ПРОФИЛЬ</h4>
<br>
  <div class="row">

    <form class="ps-product__review col-lg-6 col-md-6 col-sm-12 col-xs-12 " action="?" method="post" enctype="multipart/form-data">

      <div class="">
        <h4>ГЛАВНАЯ ИНФОРМАЦИЯ</h4>

        <div class="form-group">
          <label>Аватар:</label>

          <div class="custom-file-upload">
            <input type="file"
                   id="file"
                   name="<?php echo $formInformationForm['logoUploadFile']->vars['full_name']; ?>"
            />
          </div>

          <div class="ps-review__thumbnail"><img src="<?php echo get_avatar_url($curUser->ID); ?>" alt=""></div>

          <?php if ($formInformationForm->children['logoUploadFile']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['logoUploadFile']->vars['errors'] as $error) : ?>
              <br>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

        </div>

        <div class="form-group">
          <label>Ник:</label>

          <?php if ($formInformationForm->children['nickName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['nickName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formInformationForm['nickName']->vars['value']; ?>"
                 name="<?php echo $formInformationForm['nickName']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>

        <div class="form-group">
          <label>Имя:<span>*</span></label>

          <?php if ($formInformationForm->children['name']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['name']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formInformationForm['name']->vars['value']; ?>"
                 name="<?php echo $formInformationForm['name']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>

        <div class="form-group">
          <label>Фамилия:<span>*</span></label>

          <?php if ($formInformationForm->children['secondName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['secondName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formInformationForm['secondName']->vars['value']; ?>"
                 name="<?php echo $formInformationForm['secondName']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>

        <div class="form-group">
          <label>Телефон:<span>*</span></label>

          <?php if ($formInformationForm->children['phone']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['phone']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 data-phone
                 value="<?php echo $formInformationForm['phone']->vars['value']; ?>"
                 name="<?php echo $formInformationForm['phone']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>

        <div class="form-group">
          <label>Дополнительная информация:</label>

          <?php if ($formInformationForm->children['description']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formInformationForm->children['description']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <textarea class="form-control" name="<?php echo $formInformationForm['description']->vars['full_name']; ?>" rows="6"><?php echo $formInformationForm['description']->vars['value']; ?></textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="ps-btn ps-btn--sm">Подтвердить<i class="ps-icon-next"></i></button>
        </div>

      </div>
    </form>

    <?php if (false) : ?>
    <form class="ps-product__review col-lg-6 col-md-6 col-sm-12 col-xs-12 " action="?" method="post">
      <div class="">
        <h4>ПЛАТЕЖНАЯ ИНФОРМАЦИЯ</h4>

        <div class="form-group">
          <label>Платежное имя:</label>

          <?php if ($formBillingInformationForm->children['billingFirstName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingFirstName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingFirstName']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingFirstName']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>


        <div class="form-group">
          <label>Платежная фамилия:</label>

          <?php if ($formBillingInformationForm->children['billingSecondName']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingSecondName']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingSecondName']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingSecondName']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>



        <div class="form-group">
          <label>Платежная компания:</label>

          <?php if ($formBillingInformationForm->children['billingCompany']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingCompany']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingCompany']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingCompany']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>


        <div class="form-group">
          <label>Платежный аддресс:</label>

          <?php if ($formBillingInformationForm->children['billingAddress1']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingAddress1']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingAddress1']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingAddress1']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>




        <div class="form-group">
          <label>Платежный аддресс 2:</label>

          <?php if ($formBillingInformationForm->children['billingAddress2']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingAddress2']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingAddress2']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingAddress2']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>




        <div class="form-group">
          <label>Платежный город:</label>

          <?php if ($formBillingInformationForm->children['billingCity']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingCity']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingCity']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingCity']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>



        <div class="form-group">
          <label>Платежный индекс:</label>

          <?php if ($formBillingInformationForm->children['billingPostCode']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingPostCode']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingPostCode']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingPostCode']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>




        <div class="form-group">
          <label>Платежная страна:</label>

          <?php if ($formBillingInformationForm->children['billingCountry']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingCountry']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingCountry']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingCountry']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>


        <div class="form-group">
          <label>Платежное государство:</label>

          <?php if ($formBillingInformationForm->children['billingState']->vars['errors']->count() > 0) : ?>
            <?php foreach ($formBillingInformationForm->children['billingState']->vars['errors'] as $error) : ?>
              <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>

          <input class="form-control"
                 value="<?php echo $formBillingInformationForm['billingState']->vars['value']; ?>"
                 name="<?php echo $formBillingInformationForm['billingState']->vars['full_name']; ?>"
                 type="text"
                 placeholder="">
        </div>

        <div class="form-group">
          <button type="submit" class="ps-btn ps-btn--sm">Подтвердить<i class="ps-icon-next"></i></button>
        </div>

      </div>
    </form>
    <?php endif; ?>

  </div>
