<?php
/**
 * @var $curUser WP_User
 * @var $currUserMeta object
 */
?>

<?php
$userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id() : get_current_user_id();
$useIdFromUnloggedUser = get_current_user_id() == 0 ? true : false;

$items = Alg_WC_Wish_List::get_wish_list($userID, $useIdFromUnloggedUser);
?>

<?php if (!empty($items)) : ?>

<div class="ps-cart-listing ps-table--whishlist">
  <table class="table ps-cart__table">
    <thead>
    <tr>
      <th>Все товары</th>
      <th>Цена</th>
      <th>Просмотр</th>
      <th></th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($items as $itemID) : ?>

      <?php
      $price = get_post_meta($itemID, '_regular_price', true);
      $price_sale = get_post_meta($itemID, '_sale_price', true);
      ?>

      <tr data-wish-product="<?php echo $itemID; ?>">
        <td>
          <a class="ps-product__preview" href="product-detail.html">
            <img class="mr-15" src="<?= get_the_post_thumbnail_url($itemID, 'custom_size_100_100'); ?>" alt="">
            <?php echo get_the_title($itemID); ?>
          </a>
        </td>
        <td><?php echo $price_sale ?> грн.</td>
        <td>
          <a class="ps-product-link" href="<?php echo get_permalink( $itemID ); ?>">Посмотреть</a></td>
        <td>
          <div data-wish-product-remove="<?php echo $itemID; ?>" class="ps-remove"></div>
        </td>
      </tr>

    <?php endforeach; ?>

    </tbody>
  </table>
</div>

<?php else: ?>

<div class="ps-cart-listing ps-table--whishlist">
  Список пуст
</div>
<?php endif; ?>
