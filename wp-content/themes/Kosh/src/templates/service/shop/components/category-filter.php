<?php
/**
 * @var $categories \Symfony\Component\Form\FormView
 * @var $choice \Symfony\Component\Form\ChoiceList\View\ChoiceView
*/
?>
<?php if (isset($categories) && !empty($categories->vars['choices'])) : ?>

  <aside class="ps-widget--sidebar ps-widget--category">
  <div class="ps-widget__header">
    <h3>Категория</h3>
  </div>
  <div class="ps-widget__content" style="margin-bottom: 5px;">
    <ul class="ps-list--checked">

      <?php $count = 1; ?>
      <?php foreach ($categories->vars['choices'] as $choice) : ?>

        <li class="current <?php echo $choice->attr['class'] ?>">
          <label for="<?php echo $categories->vars['id'] . '_' . $count; ?>">
            <input data-cat-change id="<?php echo $categories->vars['id'] . '_' . $count; ?>"
                   type="checkbox"
              <?php echo in_array($choice->value, $categories->vars['value']) ? ' checked' : ''; ?>
                   name="<?php echo $categories->vars['full_name'] ?>"
                   value="<?php echo $choice->value; ?>"
            >
            <?php echo $choice->label; ?>
          </label>
        </li>

      <?php $count++; ?>
      <?php endforeach; ?>

    </ul>
  </div>
</aside>

<?php endif; ?>