<?php
/**
 * @var $sortedBy \Symfony\Component\Form\FormView
 */
?>
<?php if (isset($sortedBy) && !empty($sortedBy->vars['choices'])) : ?>

  <div class="ps-product__filter">
    <select class="ps-select selectpicker"
            name="<?php echo $sortedBy->vars['full_name'] ?>"
    >

      <?php foreach ($sortedBy->vars['choices'] as $choice) : ?>

        <option <?php echo $choice->value == $sortedBy->vars['value'] ? 'selected="selected"' : ''; ?> value="<?php echo $choice->value; ?>"><?php echo $choice->label; ?></option>

      <?php endforeach; ?>

    </select>
  </div>

<?php endif; ?>