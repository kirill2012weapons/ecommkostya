<?php
/**
 * @var $color \Symfony\Component\Form\FormView
 */
?>

<?php if (isset($color) && !empty($color->vars['choices'])) : ?>

  <aside class="ps-widget--sidebar">
    <div class="ps-widget__header">
      <h3>Цвета</h3>
    </div>
    <div class="ps-widget__content">
      <ul class="ps-list--color">

        <?php $count = 1; ?>
          <?php foreach ($color->vars['choices'] as $choice) : ?>

            <li>
              <div class="round">
                <input data-cat-change type="checkbox"
                       id="<?php echo $color->vars['id'] . '_' . $count; ?>"
                       name="<?php echo $color->vars['full_name'] ?>"
                       value="<?php echo $choice->value; ?>"
                  <?php echo in_array($choice->value, $color->vars['value']) ? ' checked' : ''; ?>
                />
                <label for="<?php echo $color->vars['id'] . '_' . $count; ?>"
                       style="<?php echo $choice->attr['style'] ?> max-width: unset;">
                </label>
              </div>
            </li>

          <?php $count++; ?>
        <?php endforeach; ?>

      </ul>
    </div>
  </aside>

<?php endif; ?>