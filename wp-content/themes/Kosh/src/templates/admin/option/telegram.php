<?php

use KoshTheme\Forms\Admin\Telega\TelegaModel;
use KoshTheme\Forms\Admin\Telega\TelegaType;

$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$telegaModel = new TelegaModel();

$telegaForm = $formFactory
  ->createBuilder(TelegaType::class, $telegaModel)
  ->getForm();

$telegaForm->handleRequest($request);

if ($telegaForm->isSubmitted() && $telegaForm->isValid()) {

  try {
    $telegram = new Longman\TelegramBot\Telegram($telegaModel->getBotApiKey(), $telegaModel->getBotName());

    $result = $telegram->setWebhook($telegaModel->getUrlWebHook());
    if ($result->isOk()) {
      $setupNewURL = true;
      $telegaModel->setIsConnected(true);
    } else {
      $setupNewURL = false;
      $telegaModel->setIsConnected(false);
    }
  } catch (Longman\TelegramBot\Exception\TelegramException $e) {
    $error = $e->getMessage();
    $setupNewURL = false;
    $telegaModel->setIsConnected(false);
  }

}

$telegaFormView = $telegaForm->createView();

$telegaFactory = container()->get('telega.bot');

?>

<div class="wrap">
  <h1>Телеграм настройки</h1>

  <?php if (!$telegaModel->getIsConnected()) : ?>
    <div class="update-message notice inline notice-error">
      <p>
        Неудалось сука подключиться к телеге
      </p>
    </div>
  <?php endif; ?>

  <?php if (isset($setupNewURL)) : ?>

    <?php if ($setupNewURL) : ?>

      <div class="update-message notice inline notice-warning notice-alt">
          <p>
          Установленые настройки норм
          </p>
      </div>

    <?php else: ?>

      <div class="update-message notice inline notice-info notice-alt">
        <p>
          Шо то не так - зови Кирюху <br>
          <?php if (isset($error)) echo $error; ?>
        </p>
      </div>

    <?php endif; ?>

  <?php endif; ?>

  <form action="?page=telegram-settings" method="post">
    <table class="form-table" role="presentation">

      <tbody>

        <tr>
          <th scope="row">
            <label for="<?= $telegaFormView->children['urlWebHook']->vars['id'] ?>">ВЕБ ХУК</label>
          </th>
          <td>

            <?php if ($telegaFormView->children['urlWebHook']->vars['errors']->count() > 0) : ?>
              <?php foreach ($telegaFormView->children['urlWebHook']->vars['errors'] as $error) : ?>
                <div style="margin-bottom: 3px;">
                  <span style="color: #ff0013; font-size: 12px;">
                      <?= $error->getMessage(); ?>
                  </span>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

            <input type="text"
                   id="<?= $telegaFormView->children['urlWebHook']->vars['id'] ?>"
                   name="<?= $telegaFormView->children['urlWebHook']->vars['full_name'] ?>"
                   value="<?= $telegaFormView->children['urlWebHook']->vars['value'] ?>"
                   class="regular-text ltr">

            <p class="description" id="new-admin-email-description">
              Юрл, конечная веб хука. Если не знаешь что это не меняй тут нечего, лучше позвони Кириллу, кек. <br>
              И вообще выйди отсюда лол
            </p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="<?= $telegaFormView->children['botName']->vars['id'] ?>">БОТ СЛАГ</label>
          </th>
          <td>

            <?php if ($telegaFormView->children['botName']->vars['errors']->count() > 0) : ?>
              <?php foreach ($telegaFormView->children['botName']->vars['errors'] as $error) : ?>
                <div style="margin-bottom: 3px;">
                  <span style="color: #ff0013; font-size: 12px;">
                      <?= $error->getMessage(); ?>
                  </span>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

            <input type="text"
                   id="<?= $telegaFormView->children['botName']->vars['id'] ?>"
                   name="<?= $telegaFormView->children['botName']->vars['full_name'] ?>"
                   value="<?= $telegaFormView->children['botName']->vars['value'] ?>"
                   class="regular-text ltr">

            <p class="description" id="new-admin-email-description">
              Нейм бота
            </p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="<?= $telegaFormView->children['botApiKey']->vars['id'] ?>">ТОКЕН БОТА</label>
          </th>
          <td>

            <?php if ($telegaFormView->children['botApiKey']->vars['errors']->count() > 0) : ?>
              <?php foreach ($telegaFormView->children['botApiKey']->vars['errors'] as $error) : ?>
                <div style="margin-bottom: 3px;">
                  <span style="color: #ff0013; font-size: 12px;">
                      <?= $error->getMessage(); ?>
                  </span>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

            <input type="text"
                   id="<?= $telegaFormView->children['botApiKey']->vars['id'] ?>"
                   name="<?= $telegaFormView->children['botApiKey']->vars['full_name'] ?>"
                   value="<?= $telegaFormView->children['botApiKey']->vars['value'] ?>"
                   class="regular-text ltr">

            <p class="description" id="new-admin-email-description">
              Токен бота
            </p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="<?= $telegaFormView->children['sales']->vars['id'] ?>">Сейлы</label>
          </th>
          <td>

            <?php if ($telegaFormView->children['sales']->vars['errors']->count() > 0) : ?>
              <?php foreach ($telegaFormView->children['sales']->vars['errors'] as $error) : ?>
                <div style="margin-bottom: 3px;">
                  <span style="color: #ff0013; font-size: 12px;">
                      <?= $error->getMessage(); ?>
                  </span>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

            <input type="text"
                   data-role="tagsinput"
                   id="<?= $telegaFormView->children['sales']->vars['id'] ?>"
                   name="<?= $telegaFormView->children['sales']->vars['full_name'] ?>"
                   value="<?= $telegaFormView->children['sales']->vars['value'] ?>"
                   class="regular-text ltr">

            <p class="description" id="new-admin-email-description-sales">
              Айдишники сейлов.
            </p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="<?= $telegaFormView->children['managers']->vars['id'] ?>">Менеджеры</label>
          </th>
          <td>

            <?php if ($telegaFormView->children['managers']->vars['errors']->count() > 0) : ?>
              <?php foreach ($telegaFormView->children['managers']->vars['errors'] as $error) : ?>
                <div style="margin-bottom: 3px;">
                  <span style="color: #ff0013; font-size: 12px;">
                      <?= $error->getMessage(); ?>
                  </span>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>

            <input type="text"
                   data-role="tagsinput"
                   id="<?= $telegaFormView->children['managers']->vars['id'] ?>"
                   name="<?= $telegaFormView->children['managers']->vars['full_name'] ?>"
                   value="<?= $telegaFormView->children['managers']->vars['value'] ?>"
                   class="regular-text ltr">

            <p class="description" id="new-admin-email-description-managers">
              Сюда кидай айдишники манагеров, которые будут получать и обрабатывать заказы
            </p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label></label>
          </th>
          <td>
            <p class="submit">
              <input style="min-height: 44px; width: 224px;" type="submit" name="submit" id="submit" class="button button-primary" value="САХАРИТЬ">
            </p>
          </td>
        </tr>

      </tbody>
    </table>


  </form>

</div>