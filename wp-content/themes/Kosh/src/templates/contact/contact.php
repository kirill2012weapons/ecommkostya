<?php

use KoshTheme\Forms\Contacts\ContactModel;
use KoshTheme\Forms\Contacts\ContactType;


$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$contactModel = new ContactModel();

if (is_user_logged_in()) {
  /**
   * @var $curUser WP_User
   */
  $curUser = wp_get_current_user();
  $currUserMeta = (object) get_user_meta($curUser->ID);
  if (isset($curUser->user_email)) $contactModel->setEmail($curUser->user_email);
  if (isset($currUserMeta->billing_phone)) $contactModel->setPhone($currUserMeta->billing_phone[0]);
}

$contactForm = $formFactory
  ->createBuilder(ContactType::class, $contactModel)
  ->getForm();

$contactForm->handleRequest($request);

if ($contactForm->isSubmitted() && $contactForm->isValid()) {
  if (sendMailCustomInfo($contactModel)) {
    wc_add_notice('Письмо отправлено.', 'success');
    wp_redirect( home_url() );
    die();
  }
  else {
    wc_add_notice('Письмо не отправлено.', 'success');
    wp_redirect( home_url() );
    die();
  }
}


$contactFormView = $contactForm->createView();

?>

<form action="?<?php echo is_page('contact') ? '' : '#contact-form'; ?>" method="post">
  <div class="form-group">
    <label>Имя<span>*</span></label>

    <?php if ($contactFormView->children['name']->vars['errors']->count() > 0) : ?>
      <?php foreach ($contactFormView->children['name']->vars['errors'] as $error) : ?>
        <div style="margin-bottom: 3px;">
          <span style="color: #ff0013; font-size: 12px;">
              <?= $error->getMessage(); ?>
          </span>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

    <input class="form-control"
           name="<?= $contactFormView->children['name']->vars['full_name'] ?>"
           value="<?= $contactFormView->children['name']->vars['value'] ?>"
           type="text">
  </div>
  <div class="form-group">
    <label>Почта<span>*</span></label>

    <?php if ($contactFormView->children['email']->vars['errors']->count() > 0) : ?>
      <?php foreach ($contactFormView->children['email']->vars['errors'] as $error) : ?>
        <div style="margin-bottom: 3px;">
          <span style="color: #ff0013; font-size: 12px;">
              <?= $error->getMessage(); ?>
          </span>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

    <input class="form-control"
           name="<?= $contactFormView->children['email']->vars['full_name'] ?>"
           value="<?= $contactFormView->children['email']->vars['value'] ?>"
           type="text">
  </div>

  <div class="form-group">
    <label>Телефон<span>*</span></label>

    <?php if ($contactFormView->children['phone']->vars['errors']->count() > 0) : ?>
      <?php foreach ($contactFormView->children['phone']->vars['errors'] as $error) : ?>
        <div style="margin-bottom: 3px;">
          <span style="color: #ff0013; font-size: 12px;">
              <?= $error->getMessage(); ?>
          </span>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

    <input class="form-control"
           name="<?= $contactFormView->children['phone']->vars['full_name'] ?>"
           value="<?= $contactFormView->children['phone']->vars['value'] ?>"
           type="text">
  </div>

  <div class="form-group">
    <label>Ваше сообщение<span>*</span></label>

    <?php if ($contactFormView->children['description']->vars['errors']->count() > 0) : ?>
      <?php foreach ($contactFormView->children['description']->vars['errors'] as $error) : ?>
        <div style="margin-bottom: 3px;">
          <span style="color: #ff0013; font-size: 12px;">
              <?= $error->getMessage(); ?>
          </span>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>

    <textarea name="<?= $contactFormView->children['description']->vars['full_name'] ?>" class="form-control" rows="4"><?= $contactFormView->children['description']->vars['value'] ?></textarea>
  </div>
  <div class="form-group text-center">
    <button type="submit" class="ps-btn">Отослать<i class="fa fa-angle-right"></i></button>
  </div>
</form>
