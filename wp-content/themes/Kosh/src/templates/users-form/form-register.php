<?php

use KoshTheme\Forms\User\Register\RegisterModel;
use KoshTheme\Forms\User\Register\RegisterType;


$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
/**
 * @var $formFactory \Symfony\Component\Form\FormFactory
 */
$formFactory = container()->get('factory.form.annotation_validator');

$registerModel = new RegisterModel();
$registerForm = $formFactory
  ->createBuilder(RegisterType::class, $registerModel)
  ->getForm();

$registerForm->handleRequest($request);

if ($registerForm->isSubmitted() && $registerForm->isValid()) {

  $userId = wp_create_user(
    $registerModel->getNickName(),
    $registerModel->getPassword(),
    $registerModel->getEmail()
  );
  wc_add_notice('Пользователь создан. Ввойдите в аккаунт.', 'success');
  update_user_meta(
    $userId,
    'first_name',
    $registerModel->getName()
  );

  update_user_meta(
    $userId,
    'last_name',
    $registerModel->getSurname()
  );

  wp_update_user( array(
    'ID'                   => $user_id,
    'billing_phone'        => $registerModel->getPhone(),
  ));

  update_user_meta($userId, 'show_admin_bar_front', 'false');

  $user = get_user_by('id', $userId);

  wp_redirect(home_url('/register/?created'));
  exit;

}

/**
 * @var $registerFormView \Symfony\Component\Form\FormView
 */
$registerFormView = $registerForm->createView();
?>

<?php if (isset($_GET['created'])) : ?>
  <h3 style="color: #20a89e;margin-bottom: 19px;">Пользователь создан.<br>Ввойдите в личный кабинет.</h3>
<?php endif; ?>

<form class="ps-contact__form" action="?" method="POST">
  <div class="row">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Ник <sub>*</sub></label>

        <?php if ($registerFormView->children['nickName']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['nickName']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                                        <span style="color: #ff0013; font-size: 12px;">
                                            <?= $error->getMessage(); ?>
                                        </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="text"
               placeholder=""
               name="<?= $registerFormView->children['nickName']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['nickName']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Имя <sub>*</sub></label>

        <?php if ($registerFormView->children['name']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['name']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                                        <span style="color: #ff0013; font-size: 12px;">
                                            <?= $error->getMessage(); ?>
                                        </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="text"
               placeholder=""
               name="<?= $registerFormView->children['name']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['name']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Фамилия</label>

        <?php if ($registerFormView->children['surname']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['surname']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                                        <span style="color: #ff0013; font-size: 12px;">
                                            <?= $error->getMessage(); ?>
                                        </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="text"
               placeholder=""
               name="<?= $registerFormView->children['surname']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['surname']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Номер телефона</label>

        <?php if ($registerFormView->children['phone']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['phone']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                <span style="color: #ff0013; font-size: 12px;">
                    <?= $error->getMessage(); ?>
                </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               data-phone
               type="text"
               placeholder="+380"
               name="<?= $registerFormView->children['phone']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['phone']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Email <sub>*</sub></label>

        <?php if ($registerFormView->children['email']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['email']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                                        <span style="color: #ff0013; font-size: 12px;">
                                            <?= $error->getMessage(); ?>
                                        </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="text"
               placeholder=""
               name="<?= $registerFormView->children['email']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['email']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <label>Пароль <sub>*</sub></label>

        <?php if ($registerFormView->children['password']->children['first']->vars['errors']->count() > 0) : ?>
          <?php foreach ($registerFormView->children['password']->children['first']->vars['errors'] as $error) : ?>
            <div style="margin-bottom: 3px;">
                                        <span style="color: #ff0013; font-size: 12px;">
                                            <?= $error->getMessage(); ?>
                                        </span>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>

        <input class="form-control"
               type="password"
               placeholder=""
               name="<?= $registerFormView->children['password']->children['first']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['password']->children['first']->vars['value'] ?>"
        >
      </div>
      <div class="form-group">
        <label>Повторите пароль <sub>*</sub></label>
        <input class="form-control"
               type="password"
               placeholder=""
               name="<?= $registerFormView->children['password']->children['second']->vars['full_name'] ?>"
               value="<?= $registerFormView->children['password']->children['second']->vars['value'] ?>"
        >
      </div>
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 ">
      <div class="form-group">
        <button class="ps-btn">Регистрация<i class="ps-icon-next"></i></button>
      </div>
    </div>
  </div>
</form>

