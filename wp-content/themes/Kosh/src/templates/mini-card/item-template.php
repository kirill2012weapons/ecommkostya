<?php

$items = WC()->cart->get_cart();

?>

<?php foreach ($items as $item => $values) : ?>

  <?php
  $product =  wc_get_product( $values['data']->get_id() );
  $getProductDetail = wc_get_product( $values['product_id'] );
  ?>

  <div class="ps-cart-item" data-item-wrapper>
    <img src="<?php echo PHP_IMG ?>/remove.svg" alt="REMOVING" style="width: 100px; text-align: center; display: none;" data-removing-toggle>
    <a class="ps-cart-item__close" data-removing-toggle data-remove-item-from-cart-mini data-item-id="<?php echo $values['data']->get_id(); ?>" href="#"></a>
    <div data-removing-toggle class="ps-cart-item__thumbnail">
      <a href="<?php echo get_post_permalink( $values['data']->get_id() ); ?>"></a>
      <?php echo $getProductDetail->get_image('custom_size_100_100'); ?>
    </div>
    <div data-removing-toggle data-removing-toggle class="ps-cart-item__content">
      <a class="ps-cart-item__title" href="<?php echo get_post_permalink( $values['data']->get_id() ); ?>"><?php echo $product->get_title() ?></a>
      <p>
        <span>Количество:<i><?php echo $values['quantity']; ?></i></span>
        <span>
        Стоимость:<i><?= $product->get_price(); ?> грн.</i>
      </span>
      </p>
    </div>
  </div>

<?php endforeach; ?>
