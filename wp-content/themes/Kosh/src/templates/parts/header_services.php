<?php
$items = get_field('mpl_g_l_r', 'opt_st_id');
?>

<?php if (!empty($items)) : ?>

  <div class="header-services">
    <div class="ps-services owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="7000" data-owl-gap="0" data-owl-nav="true"
         data-owl-dots="false" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1"
         data-owl-duration="1000" data-owl-mousedrag="on">
      <?php foreach ($items as $item) : ?>
      <p class="ps-service">

        <?php if (isset($item['logo']) && !empty($item['logo'])) : ?>
          <img style="width: 20px; display: inline-block; margin-bottom: 3px; margin-right: 3px;" src="<?php echo $item['logo']['url']; ?>" alt="logo">
        <?php endif; ?>

        <?php if (isset($item['заголовок']) && !empty($item['заголовок'])) : ?>
          <?php echo $item['заголовок']; ?>
        <?php endif; ?>

      </p>
      <?php endforeach; ?>
    </div>
  </div>

<?php endif; ?>