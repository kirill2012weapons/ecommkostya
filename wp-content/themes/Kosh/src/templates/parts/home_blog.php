<?php if ( get_field('blg_g_top_blog', 'opt_st_id') ) : ?>

  <?php
  $blogs = get_field('blg_g_top_blog', 'opt_st_id');
  ?>

  <div class="ps-section ps-home-blog pt-80 pb-80">
    <div class="ps-container">

      <div class="ps-section__header mb-50">
        <h2 class="ps-section__title" data-mask="Блог">- Новости</h2>
        <div class="ps-section__action">
          <a class="ps-morelink text-uppercase" href="<?php echo get_post_type_archive_link('blog_pt') ?>">Все новости<i class="fa fa-long-arrow-right"></i></a>
        </div>
      </div>

      <div class="ps-section__content">
        <div class="row">

          <?php foreach ($blogs as $post) : ?>

            <?php
            /**
             * @var $post WP_Post
             * @var $author WP_User
             */
            $id = $post->ID;
            $author = get_user_by('id', $post->post_author);
            ?>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              <div class="ps-post">
                <div class="ps-post__thumbnail">
                  <a class="ps-post__overlay" href="<?php echo get_post_permalink( $id ); ?>"></a>
                  <img src="<?php echo get_the_post_thumbnail_url($id, 'custom_size_958_401') ? get_the_post_thumbnail_url($id, 'custom_size_958_401') : PHP_IMG . '/placeholder.png' ?>" alt="Блог">
                </div>
                <div class="ps-post__content">
                  <a class="ps-post__title" href="<?php echo get_post_permalink( $id ); ?>">
                    <?php echo get_the_title( $id ); ?>
                  </a>
                  <p class="ps-post__meta">
                    <span>Автор:
                      <span class="mr-5"><?php echo $author->nickname; ?></span>
                    </span> -<span class="ml-5"><?php echo get_the_date('j F Y', $id); ?></span></p>
                  <p>
                    <?php echo wp_trim_words(wp_strip_all_tags( get_the_content('', false, $id) ), 45, '...'); ?>
                  </p>
                  <a class="ps-morelink" href="<?php echo get_post_permalink( $id ); ?>">Читать дальше<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </div>

          <?php endforeach; ?>

        </div>
      </div>
    </div>
  </div>

<?php endif; ?>