<?php if ( have_rows('comm_g_comm_r', 'opt_st_id') ) : ?>

  <div class="ps-home-testimonial bg--parallax pb-80" data-background="<?php echo get_field('comm_g_fn', 'opt_st_id') ? get_field('comm_g_fn', 'opt_st_id')['url'] : ''; ?>">
    <div class="container">
      <div class="owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on" data-owl-animate-in="fadeIn" data-owl-animate-out="fadeOut">

        <?php while (have_rows('comm_g_comm_r', 'opt_st_id')) : the_row(); ?>
          <div class="ps-testimonial">
          <div class="ps-testimonial__thumbnail">

            <?php if (get_sub_field('face')) : ?>

              <img src="<?= get_sub_field('face')['sizes']['thumbnail']; ?>" alt="">
              <i class="fa fa-quote-left"></i>

            <?php endif; ?>

          </div>

          <?php if (get_sub_field('who')) : ?>

            <header>
              <p><?= get_sub_field('who'); ?></p>
            </header>

          <?php endif; ?>

          <?php if (get_sub_field('who_did')) : ?>

            <footer>
              <p>“<?= get_sub_field('who_did'); ?>“</p>
            </footer>

          <?php endif; ?>

        </div>
        <?php endwhile; ?>

      </div>
    </div>
  </div>

<?php endif; ?>