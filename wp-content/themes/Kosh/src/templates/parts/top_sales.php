<?php

$products = get_field('tops_g_tov', 'opt_st_id');

?>

<?php if (!empty($products)) : ?>

 <div class="ps-section ps-section--top-sales ps-owl-root pt-80 pb-80">
  <div class="ps-container">
    <div class="ps-section__header mb-50">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
          <h3 style="width: 50% !important; min-width: 50% !important;" class="ps-section__title" data-mask="ТОП ПРОДАЖ">- Топ продаж</h3>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
          <div class="ps-owl-actions"><a class="ps-prev" href="#"><i class="ps-icon-arrow-right"></i>Назад</a><a class="ps-next" href="#">Вперед<i class="ps-icon-arrow-left"></i></a></div>
        </div>

      </div>
    </div>
    <div class="ps-section__content">
      <div class="ps-owl--colection owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="30" data-owl-nav="false" data-owl-dots="false" data-owl-item="4" data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="4" data-owl-duration="1000" data-owl-mousedrag="on">

        <?php foreach ($products as $product) : ?>

          <?php
          /**
           * @var $product WP_Post
           * @var $wcProduct WC_Product
           */
          $id = $product->ID;
          $wcProduct = $product->ID;
          ?>

         <div class="ps-shoes--carousel">
          <div class="ps-shoe">

            <div class="ps-shoe__thumbnail">

              <?php
              /**
               * Bage - NEW
               */
              $isNew = get_field('is_new', $id);
              ?>
              <?php if ($isNew) : ?>
                <div class="ps-badge">
                  <span>New</span>
                </div>
              <?php endif; ?>

              <?php
              /**
               * SALE PRISE AND %
               */
              $price = get_post_meta($id, '_regular_price', true);
              $price_sale = get_post_meta($id, '_sale_price', true);
              ?>
              <?php if ($price_sale !== '') : ?>
                <div class="ps-badge ps-badge--sale <?php echo $isNew ? 'ps-badge--2nd':''; ?>">
                  <span>-<?= ceil((($price - $price_sale) / $price) * 100) ?>%</span>
                </div>
              <?php endif; ?>

              <?php
              $userID = get_current_user_id() == 0 ? Alg_WC_Wish_List_Cookies::get_unlogged_user_id() : get_current_user_id();
              $useIdFromUnloggedUser = get_current_user_id() == 0 ? true : false;
              ?>
              <div class="ps-shoe__favorite" style="display: none;" data-wish-loader>
                <img class="lazy" src="<?php echo PHP_IMG; ?>/loadearth.svg" alt="">
              </div>
              <a class="ps-shoe__favorite <?php echo Alg_WC_Wish_List_Item::is_item_in_wish_list($id, $userID, $useIdFromUnloggedUser) ? ' in-wish' : '' ?>" href="#" data-add-to-wish-list-btn="<?php echo $id; ?>">
                <i class="ps-icon-heart"></i>
              </a>
              <img class="lazy" src="<?= get_the_post_thumbnail_url($id, 'custom_size_800_800'); ?>" alt="">
              <a class="ps-shoe__overlay" href="<?php echo get_permalink( $id ); ?>"></a>

            </div>

            <div class="ps-shoe__content">
              <div class="ps-shoe__variants">

                <?php if (get_field('gallery', get_the_ID())) : ?>

                  <?php
                  $gallery = get_field('gallery', get_the_ID());
                  ?>

                  <div class="ps-shoe__variant normal">

                    <img class="lazy" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'custom_size_800_800'); ?>" alt="">

                    <?php foreach ($gallery as $image) : ?>

                      <img class="lazy" src="<?= $image['sizes']['custom_size_800_800']; ?>" alt="">

                    <?php endforeach; ?>

                  </div>

                <?php endif; ?>

              </div>
              <div class="ps-shoe__detail"><a class="ps-shoe__name" href="#"><?= get_the_title($id); ?></a>
                <!--                                                    <p class="ps-shoe__categories">-->
                <!--                                                        <a href="#">Men shoes</a>,-->
                <!--                                                        <a href="#"> Nike</a>,-->
                <!--                                                        <a href="#"> Jordan</a>-->
                <!--                                                    </p>-->
                <span class="ps-shoe__price">
                    <?php if ($price_sale !== '') : ?>
                      <del><?= $price ?> грн.</del> <?= $price_sale ?> грн.
                    <?php else: ?>
                      <del></del><?= $price ?> грн.
                    <?php endif; ?>
                </span>
              </div>
            </div>


          </div>
        </div>

        <?php endforeach; ?>

      </div>
    </div>
  </div>
</div>

<?php endif; ?>