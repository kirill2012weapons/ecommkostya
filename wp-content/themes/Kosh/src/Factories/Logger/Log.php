<?php


namespace KoshTheme\Factories\Logger;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log
{

  /**
   * @return Logger
   */
  static public function getLogDebug()
  {

    $log = new Logger('debug_log');
    $log->pushHandler(new StreamHandler( ROOT . '/debug.log', Logger::DEBUG));

    return $log;

  }

}