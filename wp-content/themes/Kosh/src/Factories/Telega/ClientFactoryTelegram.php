<?php

namespace KoshTheme\Factories\Telega;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;

class ClientFactoryTelegram
{

  /**
   * @return \TelegramBot\Api\Client
   * @throws \Exception
   */
    static public function getClient()
    {
      if (!TELEGRAM_BOT_API_TOKEN) throw new \Exception('The KEY BOT TELEGRAM НЕ СУЩЕСТВУЕТ НАХРЕН');

        $bot = new \TelegramBot\Api\Client(TELEGRAM_BOT_API_TOKEN);

        return $bot;
    }
}