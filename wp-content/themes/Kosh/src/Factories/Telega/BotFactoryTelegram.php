<?php

namespace KoshTheme\Factories\Telega;

use KoshTheme\Forms\Admin\Telega\TelegaModel;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Exception\TelegramException;

class BotFactoryTelegram
{

    static public function getBot()
    {

      $commands_paths = [
        __DIR__ . '/../../Commands/',
      ];

      $telegaInfo = new TelegaModel();

      $bot_api_key  = $telegaInfo->getBotApiKey();
      $bot_username = $telegaInfo->getBotName();

      try {
        $telegram = new Telegram($bot_api_key, $bot_username);
        $telegram->addCommandsPaths($commands_paths);
        return $telegram;
      } catch (TelegramException $e) {
        container()->get('log.debug')->info($e->getMessage() . ' - ' . get_class(static::class));
        return null;
      }

    }

}