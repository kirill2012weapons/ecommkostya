<?php


namespace KoshTheme\Factories\NovayaPochta;


use LisDev\Delivery\NovaPoshtaApi2;

class NovayaPochtaFactory
{
  /**
   * @return NovaPoshtaApi2
   * @throws \Exception
   */
  static public function getConnection()
  {

    if (!NOVAYA_POCHTA_API_KEY) throw new \Exception('The KEY of API NovayaPochta must be provide.');

    return new NovaPoshtaApi2(NOVAYA_POCHTA_API_KEY);

  }
}