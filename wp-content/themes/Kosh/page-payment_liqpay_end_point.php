<?php
/**
 * Template Name: LIQPAY END POINT
 */
?>

<?php

if (!isset($_POST['data']) || !isset($_POST['signature'])) {
  wp_safe_redirect( home_url() );
  die();
}

$data = $_POST['data'];
$signature = $_POST['signature'];

$liqPayS = get_option('woocommerce_liqpay-webplus_settings');

$pub = $liqPayS['public_key'];
$pr = $liqPayS['private_key'];

$sign = base64_encode( sha1(
  $pr .
  $data .
  $pr
  , 1 ));

$dataDecode = base64_decode($data);
$dataDecodeObject = json_decode($dataDecode);

if ($sign != $signature) return;

if ($dataDecodeObject->status == 'success') {

  $order = new WC_Order((int) $dataDecodeObject->order_id);
  $order->update_status('completed', 'Заказ оплачен <span style="font-weight: 700; color: #09a600;">КАРТОЧКА</span>' . '<br>');
  sendMailCustomOrder($order);

} else {

  $order = new WC_Order((int) $dataDecodeObject->order_id);
  $order->update_status('failed', 'Заказ отменен <span style="font-weight: 700; color: #a60f00;">ЧЕРЕЗ КАРТОЧКУ</span>' . '<br>');

}