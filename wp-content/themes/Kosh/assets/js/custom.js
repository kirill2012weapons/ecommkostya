//Reference:
//https://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
;(function ($) {

  // Browser supports HTML5 multiple file?
  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
    isIE = /msie/i.test(navigator.userAgent);

  $.fn.customFile = function () {

    return this.each(function () {

      var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
        $wrap = $('<div class="file-upload-wrapper">'),
        $input = $('<input type="text" class="file-upload-input" />'),
        // Button that will be used in non-IE browsers
        $button = $('<button type="button" class="file-upload-button">Select a File</button>'),
        // Hack for IE
        $label = $('<label class="file-upload-button" for="' + $file[0].id + '">Select a File</label>');

      // Hide by shifting to the left so we
      // can still trigger events
      $file.css({
        position: 'absolute',
        left: '-9999px'
      });

      $wrap.insertAfter($file)
        .append((isIE ? $label : $button), $file, $input);

      // Prevent focus
      $file.attr('tabIndex', -1);
      $button.attr('tabIndex', -1);

      $button.click(function () {
        $file.focus().click(); // Open dialog
      });

      $file.change(function () {

        var files = [], fileArr, filename;

        // If multiple is supported then extract
        // all filenames from the file array
        if (multipleSupport) {
          fileArr = $file[0].files;
          for (var i = 0, len = fileArr.length; i < len; i++) {
            files.push(fileArr[i].name);
          }
          filename = files.join(', ');

          // If not supported then just take the value
          // and remove the path to just show the filename
        } else {
          filename = $file.val().split('\\').pop();
        }

        $input.val(filename) // Set the value
          .attr('title', filename) // Show filename in title tootlip
          .focus(); // Regain focus

      });

      $input.on({
        blur: function () {
          $file.trigger('blur');
        },
        keydown: function (e) {
          if (e.which === 13) { // Enter
            if (!isIE) {
              $file.trigger('click');
            }
          } else if (e.which === 8 || e.which === 46) { // Backspace & Del
            // On some browsers the value is read-only
            // with this trick we remove the old input and add
            // a clean clone with all the original events attached
            $file.replaceWith($file = $file.clone(true));
            $file.trigger('change');
            $input.val('');
          } else if (e.which === 9) { // TAB
            return;
          } else { // All other keys
            return false;
          }
        }
      });

    });

  };

  // Old browser fallback
  if (!multipleSupport) {
    $(document).on('change', 'input.customfile', function () {

      var $this = $(this),
        // Create a unique ID so we
        // can attach the label to the input
        uniqId = 'customfile_' + (new Date()).getTime(),
        $wrap = $this.parent(),

        // Filter empty input
        $inputs = $wrap.siblings().find('.file-upload-input')
          .filter(function () {
            return !this.value
          }),

        $file = $('<input type="file" id="' + uniqId + '" name="' + $this.attr('name') + '"/>');

      // 1ms timeout so it runs after all other events
      // that modify the value have triggered
      setTimeout(function () {
        // Add a new input
        if ($this.val()) {
          // Check for empty fields to prevent
          // creating new inputs when changing files
          if (!$inputs.length) {
            $wrap.after($file);
            $file.customFile();
          }
          // Remove and reorganize inputs
        } else {
          $inputs.parent().remove();
          // Move the input so it's always last on the list
          $wrap.appendTo($wrap.parent());
          $wrap.find('input').focus();
        }
      }, 1);

    });
  }

}(jQuery));

$('input[type=file]').customFile();

$(document).ready(function () {

  $('.lazy').Lazy({
    scrollDirection: 'vertical',
    effect: 'fadeIn',
    visibleOnly: true,
    onError: function(element) {
      console.log('error loading ' + element.data('src'));
    }
  });

  Inputmask("+380(9){9}").mask('[data-phone]');

  if (location.hash == 'contact-form') $("html, body").animate({ scrollTop: $('#contact-form').offset().top }, 1000);

  shopReload();

  $('.open-popup-link').magnificPopup({
    type: 'inline',
    midClick: true,
    mainClass: 'mfp-fade',
    callbacks: {
      beforeOpen: function () {
        $('[data-insert-ajax-cart-content]').html('<img src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
      },
      open: function () {
        getItemsFromCardPopup();
      }
    }
  });

  $('#np_billing_country').selectize({
    onInitialize: function () {
      this.trigger('change', this.getValue(), true);
    },
    onChange: function (value, isOnInitialize) {
      getDataByRefCountry(value);
    }
  });

  $('#np_billing_country_part').selectize();

  addItemToCardButton();
  removeItemFromCardMini();
  addToWishListListener();
  removeItemFromWishListListener();


  insertVariationPriceHtml();
  insertVariationPriceHtmlListener();
});

function insertVariationPriceHtml() {
  $jQtemplatePrice = $('[data-price-html]');
  $jQtemplatePriceChecked = $('input[data-price-html-insert]:checked');
  $jQdataProductID = $('[product-data-id]');
  if ($jQtemplatePrice.length == 0)         return;
  if ($jQtemplatePriceChecked.length == 0)  return;
  if ($jQdataProductID.length == 0)         return;


  $jQtemplatePrice.html($jQtemplatePriceChecked.attr('data-price-html-insert'));
  $jQdataProductID.attr('product-data-id', $jQtemplatePriceChecked.val());

}
function insertVariationPriceHtmlListener() {

  $jQtemplatePrice = $('[data-price-html]');
  $jQtemplatePriceChecked = $('input[data-price-html-insert]:checked');
  $jQtemplatePriceInputs = $('input[data-price-html-insert]');
  $jQdataProductID = $('[product-data-id]');
  if ($jQtemplatePrice.length == 0)         return;
  if ($jQtemplatePriceChecked.length == 0)  return;
  if ($jQtemplatePriceInputs.length == 0)  return;
  if ($jQdataProductID.length == 0)         return;

  $jQtemplatePriceInputs.on('change', function (event) {
    insertVariationPriceHtml();
  });

}

function addItemToCardButton() {

  $('[data-add-to-card]').on('click', function (event) {
    event.preventDefault();

    $btn = $(this);

    $itemID = $(this).attr('product-data-id');
    $itemCount = $('[product-data-count]').val();

    var $data = {
      action: 'add_to_card_item',
      item_id: $itemID,
      item_count: $itemCount
    };

    $.ajax({
      url: myajax.url,
      type: "POST",
      data: $data,
      dataType: 'json',
      beforeSend: function (xhr) {
        $('[data-triger-display]').slideToggle();
      },
      error: function (xhr, status, error) {
        $('[data-triger-display]').slideToggle();
      },
      success: function (result, status, xhr) {
        if (result.success === true) {
          $('[data-triger-display]').slideToggle();
          $('[data-count-item-mini-cart]').html(result.data.count);
          $('[data-total-item-cost-cart]').html(result.data.total_sum);
          $('[data-item-content-wrapper]').html(result.data.items_html);
          removeItemFromCardMini();
          $('[href="#cart-popup"]').click();
        }
      }
    })

  })

}

function removeItemFromCardMini() {

  $('[data-remove-item-from-cart-mini]').on('click', function (event) {
    event.preventDefault();

    $btn = $(this);

    $itemID = $(this).attr('data-item-id');

    var $data = {
      action: 'remove_from_card_item',
      item_id: $itemID
    };

    $.ajax({
      url: myajax.url,
      type: "POST",
      data: $data,
      dataType: 'json',
      beforeSend: function (xhr) {
        $btn.closest('[data-item-wrapper]').find('[data-removing-toggle]').slideToggle();
      },
      error: function (xhr, status, error) {
        $btn.closest('[data-item-wrapper]').find('[data-removing-toggle]').slideToggle();
      },
      success: function (result, status, xhr) {
        if (result.success === true) {
          $('[data-count-item-mini-cart]').html(result.data.count).show(100);
          $('[data-total-item-cost-cart]').html(result.data.total_sum).show(100);
          $('[data-item-content-wrapper]').html(result.data.items_html).show(100);
          removeItemFromCardMini();
          updateCheckoutForm();
        }
      }
    })

  })

}

function getDataByRefCountry(value) {

  $btn = $(this);

  $valRef = value;

  var $data = {
    action: 'get_all_parts_by_ref',
    ref: $valRef
  };

  if (!$valRef) {
    $('#np_billing_country_part').html('<option value="">Выберете Населенный пункт</option>');
    return;
  }

  $.ajax({
    url: myajax.url,
    type: "POST",
    data: $data,
    dataType: 'json',
    beforeSend: function (xhr) {
      $('#np_billing_country_part ~ .selectize-control').slideToggle();
      $('[data-trigger-load-parts-nv]').slideToggle();
    },
    error: function (xhr, status, error) {
      $('#np_billing_country_part ~ .selectize-control').slideToggle();
      $('[data-trigger-load-parts-nv]').slideToggle();
    },
    success: function (result, status, xhr) {
      if (result.success === true) {
        $btn[0].value = '';
        $select = $('#np_billing_country_part');
        $select.html('');
        $string = '';
        for (var i = 0; i < result.data.length; i++) {
          $string += '<option value="' + result.data[i].ref + '">' + result.data[i].DescriptionRu + '</option>';
        }
        $select.selectize()[0].selectize.destroy();
        $select.html($string);
        $select.selectize();
      }
      $('#np_billing_country_part ~ .selectize-control').slideToggle();
      $('#np_billing_country_part ~ .selectize-control').slideToggle();
      $('[data-trigger-load-parts-nv]').slideToggle();
    }
  })


}

function getItemsFromCardPopup() {

  var $data = {
    action: 'get_items_from_card_popup'
  };

  $.ajax({
    url: myajax.url,
    type: "POST",
    data: $data,
    dataType: 'json',
    beforeSend: function (xhr) {
    },
    error: function (xhr, status, error) {
    },
    success: function (result, status, xhr) {
      if (result.success === true) {
        $('[data-insert-ajax-cart-content]').html(result.data);
        addListenersToCardPopup();
      }
    }
  })


}

function changeItemsFromCardPopup($itemID, $itemCount) {


  var $data = {
    action: 'change_items_from_card_popup',
    item_id: $itemID,
    item_count: $itemCount
  };

  $.ajax({
    url: myajax.url,
    type: "POST",
    data: $data,
    dataType: 'json',
    beforeSend: function (xhr) {
      $('[data-insert-ajax-cart-content]').html('<img src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
    },
    error: function (xhr, status, error) {
      $('[data-insert-ajax-cart-content]').html('<img src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
    },
    success: function (result, status, xhr) {
      if (result.success === true) {
        getItemsFromCardPopup();
        getItemsFromCardMini();
        updateCheckoutForm();
      }
    }
  })


}

function getItemsFromCardMini() {

  var $data = {
    action: 'get_items_from_card_mini',
  };

  $.ajax({
    url: myajax.url,
    type: "POST",
    data: $data,
    dataType: 'json',
    beforeSend: function (xhr) {
    },
    error: function (xhr, status, error) {
    },
    success: function (result, status, xhr) {
      if (result.success === true) {
        $('.ps-cart').closest('div').html(result.data);
        removeItemFromCardMini();
      }
    }
  })


}

function addListenersToCardPopup() {
  $('.table.ps-cart__table [data-it-cart-popup]').on('change', function (e) {
    e.preventDefault();
    if (+$(this).val() >= 1) {
      changeItemsFromCardPopup($(this).attr('data-it-cart-popup'), +$(this).val());
    } else {
      changeItemsFromCardPopup($(this).attr('data-it-cart-popup'), 1);
    }
  });

  $('.table.ps-cart__table .ps-remove').on('click', function (event) {
    event.preventDefault();

    $btn = $(this);

    $itemID = $(this).closest('table').find('[data-it-cart-popup]').attr('data-it-cart-popup');

    var $data = {
      action: 'remove_from_card_item',
      item_id: $itemID
    };

    $.ajax({
      url: myajax.url,
      type: "POST",
      data: $data,
      dataType: 'json',
      beforeSend: function (xhr) {
        $('[data-insert-ajax-cart-content]').html('<img src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
      },
      error: function (xhr, status, error) {
        $('[data-insert-ajax-cart-content]').html('<img src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
      },
      success: function (result, status, xhr) {
        if (result.success === true) {
          getItemsFromCardPopup();
          getItemsFromCardMini();
          updateCheckoutForm();
        }
      }
    })

  });

  $('[data-remove-item-popup-1]').on('click', function (e) {
    e.preventDefault();
    $input = $(this).closest('.form-group--number').find('[data-it-cart-popup]')[0];
    if ((+$input.value - 1) >= 1) {
      $input.value = +$input.value - 1;
      $('.table.ps-cart__table [data-it-cart-popup="' + $input.getAttribute('data-it-cart-popup') + '"]').trigger('change');
    }
  });
  $('[data-add-item-popup-1]').on('click', function (e) {
    e.preventDefault();
    $input = $(this).closest('.form-group--number').find('[data-it-cart-popup]')[0];
    if ((+$input.value + 1) >= 1) {
      $input.value = +$input.value + 1;
      $('.table.ps-cart__table [data-it-cart-popup="' + $input.getAttribute('data-it-cart-popup') + '"]').trigger('change');
    }
  });
}


function updateCheckoutForm() {

  var $data = {
    action: 'update_checkout_form',
  };

  $.ajax({
    url: myajax.url,
    type: "POST",
    data: $data,
    dataType: 'json',
    beforeSend: function (xhr) {
      $('form.ps-checkout__form .ps-checkout__order').html('<img style="display: block;margin-left: auto;margin-right: auto;" src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
    },
    error: function (xhr, status, error) {
      $('form.ps-checkout__form .ps-checkout__order').html('<img style="display: block;margin-left: auto;margin-right: auto;" src="/wp-content/themes/Kosh/assets/images/loadearth.svg">');
    },
    success: function (result, status, xhr) {
      if (result.success === true) {
        $('form.ps-checkout__form .ps-checkout__order').closest('div').html(result.data);
      }
    }
  })


}

function addToWishListListener() {

  $('[data-add-to-wish-list-btn]').on('click', function (e) {
    e.preventDefault();

    $aBtn = $(this);

    $id = $(this).attr('data-add-to-wish-list-btn');
    var $data = {
      action: 'add_wish_item',
      item_id: $id
    };

    $.ajax({
      url: myajax.url,
      type: "POST",
      data: $data,
      dataType: 'json',
      beforeSend: function (xhr) {
        $aBtn.closest('div').find('[data-wish-loader]').slideToggle();
        $aBtn.slideToggle();
      },
      error: function (xhr, status, error) {
        $aBtn.closest('div').find('[data-wish-loader]').slideToggle();
        $aBtn.slideToggle();
      },
      success: function (result, status, xhr) {
        if (result.success === true) {
          if ($aBtn.hasClass('in-wish')) {
            $aBtn.removeClass('in-wish');
            $(document).find('[data-add-to-wish-list-btn="' + $id + '"]').removeClass('in-wish');
          } else {
            $aBtn.addClass('in-wish');
            $(document).find('[data-add-to-wish-list-btn="' + $id + '"]').addClass('in-wish');
          }
          $aBtn.closest('div').find('[data-wish-loader]').slideToggle();
          $aBtn.slideToggle();
        }
      }
    })
  });

}


function removeItemFromWishListListener() {

  $('[data-wish-product-remove]').on('click', function (e) {
    e.preventDefault();

    $aBtn = $(this);

    $aBtnWrapp = $aBtn.closest('[data-wish-product]');

    $id = $(this).attr('data-wish-product-remove');
    var $data = {
      action: 'add_wish_item',
      item_id: $id
    };

    $.ajax({
      url: myajax.url,
      type: "POST",
      data: $data,
      dataType: 'json',
      beforeSend: function (xhr) {
        $aBtn.closest('[data-wish-product]').html('<img style="height: 100px;" src="/wp-content/themes/Kosh/assets/images/remove.svg" alt="">');
      },
      error: function (xhr, status, error) {
        $aBtnWrapp.remove();
      },
      success: function (result, status, xhr) {
        if (result.success === true) {
          $aBtnWrapp.remove();
        }
      }
    })
  });

}

function shopReload() {

  $('[name="fltr[sortedBy]"]').on('change', function (e) {
    $('[data-form-sbm-load-triger]').slideToggle();
    $('form[data-form-sbm]').submit();
  });
  $('[data-cat-change]').on('change', function (e) {
    $('[data-form-sbm-load-triger]').slideToggle();
    $('form[data-form-sbm]').submit();
  });

}