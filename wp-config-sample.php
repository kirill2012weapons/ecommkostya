<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Composer\Autoload\ClassLoader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;

/** @var ClassLoader $loader */
$loader = require __DIR__ . './vendor/autoload.php';

define('ROOT', __DIR__);

define('DB_NAME', 'kosh');
define('DB_USER', 'alooner');
define('DB_PASSWORD', 'alooner');
define('DB_HOST', '127.0.0.1');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('WP_SITEURL', 'https://kirill-n7n3.localhost.run');
define('WP_HOME', 'https://kirill-n7n3.localhost.run');
//define('WP_SITEURL', 'https://root-rjev.localhost.run');
//define('WP_HOME', 'https://root-rjev.localhost.run');

define('NOVAYA_POCHTA_API_KEY', 'a8272d235535dfd5e2e3eb8cf03c0e13');

//define( 'WP_CACHE', true );

/**
 *  t.me/kirya_shop_bot
 */
define('TELEGRAM_BOT_API_TOKEN', '1045468673:AAHPXHWWD-sSnlbn6gu1P4F6bPfrk4ZkJa4');

define('AUTH_KEY', 'rXLD[l[G5Wat@sv}]L,=/Q^[NkCJU|je[b!HU))v0^9F+~<as0`KP|PW0oyJ=wDH');
define('SECURE_AUTH_KEY', 'CM[E3RO`)=Out_dY.Puys=P-{7YhS@M#/t^AclEzy!zwCTb<-5t NM>aQd*o|VFs');
define('LOGGED_IN_KEY', '0J|=-N1}Hp&cJ#|`Rco&WIG)6322t+?p-rr--v58K(:$U%@w48hN`ZWl8Z+ixU-/');
define('NONCE_KEY', '[0 xe/]MaV+pnVfQ>{K7{?f-|J4#PILyjyreeht*S H-*C/_,97d`B;KMk3h|n`6');
define('AUTH_SALT', '+)|%}~}P?o}DhgVz30]C$y[>`Aa_52~`3DIGi9|,k9|D%Iys+S`OiL%^N`XNokkq');
define('SECURE_AUTH_SALT', 'BtMPjWp5P,02,{4L-QIpC-EgJemJal&+IIFiA_[7v|[C]p`oo?|>**:N;~0S|<PA');
define('LOGGED_IN_SALT', '>;{Tvf&m+&MfOdH$=|4u_+(i$N~|poIc)sD.M]^hb^^/zp_Ap1R54sb*!V}sDE!4');
define('NONCE_SALT', 'N1ei0@r7CMf/d9b*Z?2wH]5j2=^B8hBE67B+}+3QBZQV,r;o4231>)-<E)V;HGwG');

$table_prefix = 'wp_';

/**
 * TEMPLATING START
 */
$filesystemLoader = new FilesystemLoader(__DIR__ . '/wp-content/themes/Kosh/src/templates/%name%');

global $templating;

$templating = new PhpEngine(new TemplateNameParser(), $filesystemLoader);

/**
 * @param $pathFile string
 * @param $args array
 * @return false|string
 */
function render($pathFile, $args = []) {
  global $templating;
  return $templating->render($pathFile, $args);
}

/**
 * TEMPLATING END
 */

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

global $containerBuilder;
$containerBuilder = new ContainerBuilder();
$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__));

try {
  $loader->load('config/services.yaml');
} catch (\Exception $exception) {
  dump($exception->getMessage());
  exit();
}

/**
 * @return ContainerBuilder
 */
function container() {
  global $containerBuilder;
  return $containerBuilder;
}

define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', true);
define( 'WP_DEBUG_LOG', true );

if (!defined('ABSPATH')) {
  define('ABSPATH', dirname(__FILE__) . '/');
}

require_once(ABSPATH . 'wp-settings.php');
